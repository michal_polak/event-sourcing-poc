FROM openjdk:16-jdk-alpine
ARG JAR_FILE=build/libs/event-sourcing.jar
COPY ${JAR_FILE} app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app.jar"]