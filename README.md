# Event Sourcing Spring Boot Starter

The starter for help to introduce event-sourcing in your Spring Boot applications. It has the following main
dependencies:

- Spring Boot (2.5.4)
- Jackson 2.12.5
- Lombok 1.18.20

## Getting start

Add dependency in `build.gradle` of your project

```groovy
implementation 'lib.starter.eventsourcing:spring-boot-eventsourcing-starter:${version}'
```

Import configuration for starter in your application.

```java 
@Import(EventSourcingAutoConfiguration.class)
```

Enable JPA repositories and scan entities in starter

```java
@EnableJpaRepositories(basePackages = {EventSourcingAutoConfiguration.BASE_PACKAGE})
@EntityScan(basePackages = {EventSourcingAutoConfiguration.BASE_PACKAGE})
```

Enable async for asynchronus event handling

```
@EnbaleAsync
```

Enable scheduling for schedule events processing

```
@EnableScheduling
```

In file [changelog-genesis.xml](/src/resources/db.changelog/changelog-genesis.xml) you can find sql scripts for prepare
required tables in database. It was prepared for postgresql

## Features

### Event upcasting

Upcaster is te gate where foreign events are introducing to event sourcing machine. Your external event should
implement `UpcastableEvent` if you want to use event sourcing. If default upcaster is not enaught can implement custom
upcaster for your event. It is a good place to normalize event data and avoid event versioning. Every event verion
should be cated to newest

```java

@Component
@RequiredArgsConstructor
public class YourUpcaster implements EventUpcaster<ExampleChangedExternalEvent> {

    final ObjectMapper objectMapper;

    @Override
    public MutableEventData upcast(final YourExternalEvent externalEvent) {
        return MutableEventData.of(objectMapper, externalEvent);
    }
}
```

`MutableEventData` is class to manipulate event before saving as internal event

Upcasters registration

```java

@Configuration
@RequiredArgsConstructor
public class EventUpcastConfiguration extends EventUpcastConfigurer {
    private final YourUpcaster yourUpcaster;

    @Override
    public void registerUpcasters(final EventUpcastContext context) {
        context.register(YourUpcastableEvent.class, yourUpcaster);
    }
}
```

### Event queuing

Your events should extends `AbstractInternalEvent`
When you want to publish event inject been `InternalEventPublisher` and use method `.publish()`. Event will be stored in
database and in next cycle will be published as usual spring event. You can catch it using @EventListener or
@TransactionalEventListener.

### Command queuing

Your events should extends `InternaCommand`
When you want to publish event inject been `InternaCommandPublisher` and use method `.publish()`. Event will be stored
in database and in next cycle will be published as usual spring event. You can catch it using @EventListener

### Data squashing

Squashing is a process of combining data from many events to determine state of aggregate Squashing process
need `SquashKneader` to combine single event data to general state determined from previous events.
Default `SquashKneader` just override or add values by values from next event. Each squashing need new `SquashKneader`
You can implement `SquashKneader` interface and `SquashKneaderCreator` and register by `SquashingConfigurer` for
specific aggregate.

```java

@Configuration
@RequiredArgsConstructor
public class SquashingConfiguration extends SquashingConfigurer {
    private final YourKneaderCreator yourKneaderCreator;

    @Override
    public void registerUpcasters(final EventUpcastContext context) {
        context.register(YourAgrregateData.class, yourKneaderCreator);
    }
}

```

### Data snapshotting

Data snapshtting let optimize squashing data. When aggregate has many events is hard to quick determine state of
aggregate. When you make snapshot of aggregate state squashing wil be faster because will using last snapshot data as
start point of squashing.

You can make snapshot on demand. Just publish `MakeDataSnapshotCommand`

```java

@Component
@RequiredArgsConstructor
public class YourSnapshoter {

    private final SnapshotDataProvider snapshotDataProvider;
    private final InternalCommandPublisher commandPublisher;

    @Async
    @EventListener
    public void handle(final AbstractInternalEvent event) {
        if (itIsGoodTimeToMakeASnapshot(event)) {
            final String contractId = ContractId.Extractor.forClass(YourAggregateData.class);
            final MakeDataSnapshotCommand command = new MakeDataSnapshotCommand(event.getEventName(), contractId, event.getEventIndex());
            commandPublisher.publishCommand(event.getAggregateId(), event.getAggregateVersion(), command);
        }
    }
}
```

### Multiple event handler

Now with multiple events handler you can react only if few required events occurred.

Define you multiple event handler like this:

```java

@Component
@RequiredArgsConstructor
@ContractId("YourMultiEventHandler")
public class YourMultiEventHandler extends MultiEventHandler {

    @Override
    protected List<String> getEventNames() {
        return List.of("EventOne", "EventTwo", "EventThree");
    }

    @Override
    protected void handle(List<AbstractInternalEvent> events) {
        // do something
    }
}
```

Register your multiple event handler

```java

@Configuration
public class MultiEventConfiguration extends MultiEventHandlerConfigurer {

    @Autowired
    private YourMultiEventHandler yourMultiEventHandler;

    @Override
    public void registerHandlers(MultiEventHandlerContext context) {
        context.register(yourMultiEventHandleró);
    }
}

```

### GDPR Support
GDPR often force to remove personal data after close user account.
If you need to be GDPR compliance, you should remove personal data also from events.
Event payload is usually saved as JSON. Is hard find each personal data and rewrite payloads.
Now you can automatically encrypt personal data during json serialization and encrypt during deserialization. 
Generate key pairs by `SecretService` when new user appears in your system.
Wrap the sensitive value in `SecretString` before store in database. 
You should use it in each class what is serialized and stored as json. 

```java
final UUID keyId = secretService.getOrGenerateKeyPair(request.getUserId());
final SecretString firstName = new SecretString(keyId, request.getFirstName());
final SecretString lastName = new SecretString(keyId, request.getLastName());
```
When you need anonymize personal data, just drop private keys for specific user.
```java
secretService.dropPrivateKeys(userId)
```

### Extras

Wait for data - Additional command `WaitForDataCommand` can help you when you need to do something but required data is
not available at this moment you can publish this command and handle callback events `MissingDataProvidedInternalEvent`
and `MissingDataUndeliveredInternalEvent`.
