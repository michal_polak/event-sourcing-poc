package lib.starter.serialization;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeConverter;
import java.io.IOException;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
public class JsonConverter implements AttributeConverter<Object, String> {
    private final ObjectMapper objectMapper = ObjectMapperConfiguration.objectMapper();

    @Override
    public String convertToDatabaseColumn(final Object data) {

        String customerInfoJson = null;
        try {
            customerInfoJson = objectMapper.writeValueAsString(data);
        } catch (final JsonProcessingException e) {
            log.error("JSON writing error", e);
        }

        return customerInfoJson;
    }

    @Override
    public Object convertToEntityAttribute(final String json) {

        Map<String, Object> customerInfo = null;
        try {
            customerInfo = objectMapper.readValue(json, Map.class);
        } catch (final IOException e) {
            log.error("JSON reading error", e);
        }

        return customerInfo;
    }

}