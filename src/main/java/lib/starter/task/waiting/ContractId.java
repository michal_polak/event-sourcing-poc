package lib.starter.task.waiting;


import org.reflections.Reflections;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import java.util.Optional;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ContractId {
    String value();

    class Extractor {
        public static String forClass(Class<?> clazz) {
            return clazz.getAnnotation(ContractId.class).value();
        }
    }

    class Scanner {
        public static Optional<Class<?>> classByContractId(final String contractId, final List<String> basePackages) {
            Reflections reflections = new Reflections(basePackages);
            return reflections.getTypesAnnotatedWith(ContractId.class)
                    .stream()
                    .filter((clazz) -> filterByAnnotationValue(contractId, clazz)).findFirst();
        }

        private static boolean filterByAnnotationValue(String contractId, Class<?> clazz) {
            return clazz.getAnnotation(ContractId.class).value().equals(contractId);
        }
    }

}
