package lib.starter.task.waiting;

import com.fasterxml.jackson.databind.ObjectMapper;
import lib.starter.event.internal.InternalEventPublisher;
import lib.starter.sqush.DataProvider;
import lib.starter.sqush.SquashedData;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@RequiredArgsConstructor
public class WaitForDataCommandExecutor {

    private final ObjectMapper objectMapper;
    private final DataProvider dataProvider;
    private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    private final InternalEventPublisher publisher;
    private final List<String> packagesToScan;

    @Async
    @EventListener
    void execute(final WaitForDataCommand command) {
        if (command.getWaitTime() != null) {
            long delay = command.getWaitTime().toMillis();
            executor.schedule(() -> analyzeMissingData(command), delay, TimeUnit.MILLISECONDS);
        } else {
            analyzeMissingData(command);
        }
    }

    private void analyzeMissingData(WaitForDataCommand command) {
        final Class<?> clazz = searchDataClass(command);
        final SquashedData<?> squashedData = dataProvider.getCurrentData(command.getAggregateId(), command.getAggregateVersion(), clazz);
        final Map<String, Object> data = objectMapper.convertValue(squashedData.getData(), Map.class);
        if (isMissingDataProvided(command, data)) {
            final MissingDataProvidedInternalEvent event = new MissingDataProvidedInternalEvent(command.getOriginalCause(), command.getFields());
            publisher.publishEvent(squashedData.getAggregateId(), squashedData.getAggregateVersion(), event);
        } else {
            final MissingDataUndeliveredInternalEvent event = new MissingDataUndeliveredInternalEvent(command.getOriginalCause(), command.getFields());
            publisher.publishEvent(squashedData.getAggregateId(), squashedData.getAggregateVersion(), event);
        }
    }

    private Class<?> searchDataClass(WaitForDataCommand command) {
        return ContractId.Scanner.classByContractId(command.getContractId(), packagesToScan).orElseThrow(() -> new RuntimeException(""));
    }

    private boolean isMissingDataProvided(WaitForDataCommand command, Map<String, Object> data) {
        return command.getFields().stream().allMatch(key -> data.get(key) != null);
    }
}
