package lib.starter.task.waiting;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.jackson.Jacksonized;
import org.springframework.boot.convert.DurationFormat;
import org.springframework.boot.convert.DurationStyle;
import lib.starter.command.AbstractInternalCommand;

import java.time.Duration;
import java.util.List;

@Getter
@Builder
@Jacksonized
@RequiredArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class WaitForDataCommand extends AbstractInternalCommand {

    @JsonProperty("@waitTime")
    @DurationFormat(DurationStyle.ISO8601)
    private final Duration waitTime;

    @JsonProperty("@originalCause")
    private final String originalCause;

    @JsonProperty("@fields")
    private final List<String> fields;

    @JsonProperty("@contractId")
    private final String contractId;

}
