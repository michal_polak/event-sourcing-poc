package lib.starter.task.waiting;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.jackson.Jacksonized;
import lib.starter.event.internal.AbstractInternalEvent;

import java.util.List;

@Getter
@Builder
@Jacksonized
@RequiredArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class MissingDataProvidedInternalEvent extends AbstractInternalEvent {

    @JsonProperty("@originalCause")
    private final String originalCause;
    @JsonProperty("@fields")
    private final List<String> fields;
}
