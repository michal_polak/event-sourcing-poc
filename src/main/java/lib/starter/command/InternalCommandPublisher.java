package lib.starter.command;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
public class InternalCommandPublisher {

    private final InternalCommandContext commandContext;
    private final ObjectMapper objectMapper;
    private final InternalCommandRepository repository;

    public void publishCommand(final String aggregateId, final Long aggregateVersion, final AbstractInternalCommand command) {
        try {
            final String name = commandContext.findName(command.getClass());
            final String payload = objectMapper.writeValueAsString(command);
            final InternalCommandEntity commandEntity = InternalCommandEntity.of(aggregateId, aggregateVersion, payload, name);
            repository.saveAndFlush(commandEntity);
        } catch (Exception e) {
            log.error("Command not published! Problem with serialize to JSON.", e);
        }
    }
}
