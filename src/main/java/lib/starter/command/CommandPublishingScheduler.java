package lib.starter.command;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lib.starter.pointer.PointerKeeper;
import lib.starter.pointer.PointerName;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@RequiredArgsConstructor
public class CommandPublishingScheduler {

    private final InternalCommandContext commandContext;
    private final InternalCommandRepository repository;
    private final ApplicationEventPublisher publisher;
    private final ObjectMapper objectMapper;
    private final PointerKeeper pointerKeeper;
    private final AtomicLong pointer = new AtomicLong();
    private final int packageSize;

    @Scheduled(
            initialDelayString = "${starter.eventsourcing.command.publishing.init-delay}",
            fixedRateString = "${starter.eventsourcing.command.publishing.fixed-rate}"
    )
    void run() {
        final Pageable pageable = PageRequest.of(0, packageSize, Sort.by("index"));
        final Page<InternalCommandEntity> page = repository.findByIndexIsGreaterThan(pointer.longValue(), pageable);
        final List<InternalCommandEntity> commandEntities = page.getContent();
        commandEntities.forEach(ce -> {
            try {
                final Class<? extends AbstractInternalCommand> clazz = commandContext.findClass(ce.getName());
                final AbstractInternalCommand command = objectMapper.readValue(ce.getPayload(), clazz);
                final Metadata metaData = ce.metaData();
                command.includeMetadata(metaData);
                publisher.publishEvent(command);
                log.info("Command '" + ce.getName() + "' with id '" + ce.getId() + "'  published.");
                pointer.set(ce.getIndex());
            } catch (JsonProcessingException e) {
                log.error("Command '" + ce.getName() + "' with id '" + ce.getId() + "' contract is broken!", e);
            }
            pointerKeeper.update(PointerName.COMMAND_QUEUE, pointer);
        });
    }

    @PostConstruct
    private void postConstruct() {
        final Long startIndex = pointerKeeper.findOrCreatePointerIndex(PointerName.COMMAND_QUEUE);
        pointer.set(startIndex);
    }

}
