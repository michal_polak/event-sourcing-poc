package lib.starter.command;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;

@Getter
public class AbstractInternalCommand {

    @JsonIgnore
    private Metadata metadata;

    public void includeMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    @JsonIgnore
    public String getAggregateId() {
        return metadata.getAggregateId();
    }

    @JsonIgnore
    public Long getAggregateVersion() {
        return metadata.getAggregateVersion();
    }

}
