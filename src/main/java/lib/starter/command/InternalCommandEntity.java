package lib.starter.command;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.UUID;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "InternalCommandEntity")
@Table(name = "internal_command")
public class InternalCommandEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private Long index;

    @Column
    private String name;

    @Column
    private String aggregateId;

    @Column
    private Long aggregateVersion;

    @Column
    private ZonedDateTime occurredOn;

    @Column
    private String payload;

    @Column
    private ZonedDateTime creationDate;

    public static InternalCommandEntity of(java.lang.String aggregateId, Long aggregateVersion, String payload, String name) {
        return InternalCommandEntity.builder()
                .aggregateId(aggregateId)
                .aggregateVersion(aggregateVersion)
                .occurredOn(ZonedDateTime.now())
                .payload(payload)
                .name(name)
                .build();
    }

    public Metadata metaData() {
        return new Metadata(getId(), getAggregateId(), getAggregateVersion(), getOccurredOn(), getIndex());
    }


    @PrePersist
    private void prePersist() {
        id = UUID.randomUUID();
        creationDate = ZonedDateTime.now();
    }


}
