package lib.starter.command;

import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
public class InternalCommandContext {

    final Map<String, Class<? extends AbstractInternalCommand>> mapping = new HashMap<>();

    public void register(String eventName, Class<? extends AbstractInternalCommand> clazz) {
        mapping.put(eventName, clazz);
    }

    public Class<? extends AbstractInternalCommand> findClass(String commandName) {
        return mapping.get(commandName);
    }

    public String findName(Class<? extends AbstractInternalCommand> clazz) {
        return mapping.entrySet().stream().filter(e -> e.getValue() == clazz).findFirst().map(Map.Entry::getKey).orElse(null);
    }
}
