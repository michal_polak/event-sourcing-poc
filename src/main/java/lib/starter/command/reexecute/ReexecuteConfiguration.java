package lib.starter.command.reexecute;

import com.fasterxml.jackson.databind.ObjectMapper;
import lib.starter.command.InternalCommandContext;
import lib.starter.command.InternalCommandRepository;
import lib.starter.event.internal.InternalEventContext;
import lib.starter.event.internal.InternalEventRepository;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ReexecuteConfiguration {


    @Bean
    ReexecuteCommandCommandExecutor reexecuteCommandCommandExecutor(final InternalCommandContext internalCommandContext,
                                                                 final InternalCommandRepository internalCommandRepository,
                                                                 final ObjectMapper objectMapper,
                                                                 final ApplicationContext applicationContext
    ) {

        return new ReexecuteCommandCommandExecutor(
                internalCommandContext,
                internalCommandRepository,
                objectMapper,
                applicationContext
        );

    }


}
