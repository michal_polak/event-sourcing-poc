package lib.starter.command.reexecute;

import com.fasterxml.jackson.annotation.JsonProperty;
import lib.starter.command.AbstractInternalCommand;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Getter
@RequiredArgsConstructor
public class ReexecuteCommand extends AbstractInternalCommand {

    @JsonProperty("@commandId")
    private final UUID commandId;

    @JsonProperty("@executorContractId")
    private final String executorContractId;
}
