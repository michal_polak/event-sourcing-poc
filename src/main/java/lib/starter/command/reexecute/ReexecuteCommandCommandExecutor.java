package lib.starter.command.reexecute;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lib.starter.command.*;
import lib.starter.task.waiting.ContractId;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.transaction.event.TransactionalEventListener;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@RequiredArgsConstructor
public class ReexecuteCommandCommandExecutor {

    private final InternalCommandContext internalCommandContext;
    private final InternalCommandRepository internalEventRepository;
    private final ObjectMapper objectMapper;
    private final ApplicationContext applicationContext;

    @EventListener
    public void execute(final ReexecuteCommand command) throws IllegalAccessException, InvocationTargetException, JsonProcessingException {
        final InternalCommandEntity commandEntity = internalEventRepository.findById(command.getCommandId()).orElseThrow(() -> new RuntimeException("Event not found!"));
        final Class<? extends AbstractInternalCommand> clazz = internalCommandContext.findClass(commandEntity.getName());
        final AbstractInternalCommand event = objectMapper.readValue(commandEntity.getPayload(), clazz);
        final Metadata metaData = commandEntity.metaData();
        event.includeMetadata(metaData);
        final Optional<Class<?>> optionalHandlerClass = ContractId.Scanner.classByContractId(command.getExecutorContractId(), List.of("fake.application", "lib.starter"));

        if (optionalHandlerClass.isPresent()) {
            final Class<?> handlerClass = optionalHandlerClass.get();
            final Object handler = applicationContext.getBean(handlerClass);
            final Method executingMethod = findHandingMethod(command, commandEntity, clazz, handlerClass);
            executingMethod.invoke(handler, event);
        }
    }

    private Method findHandingMethod(ReexecuteCommand command, InternalCommandEntity commandEntity, Class<? extends AbstractInternalCommand> clazz, Class<?> executorClass) {
        return Arrays.stream(executorClass.getDeclaredMethods()).filter(m -> isExecutingMethod(clazz, m)).findFirst()
                .orElseThrow(() -> new IllegalStateException("Not found handling method for event " +
                        "'" + commandEntity.getName() + "' in handler '" + command.getExecutorContractId() + "'."));
    }

    private boolean isExecutingMethod(Class<? extends AbstractInternalCommand> clazz, Method m) {
        return Arrays.asList(m.getParameterTypes()).contains(clazz) && m.getParameterCount() == 1 &&
                (m.getAnnotationsByType(EventListener.class).length > 0
                        || m.getAnnotationsByType(TransactionalEventListener.class).length > 0);
    }
}
