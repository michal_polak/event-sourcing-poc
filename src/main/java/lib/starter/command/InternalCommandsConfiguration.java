package lib.starter.command;


import com.fasterxml.jackson.databind.ObjectMapper;
import lib.starter.event.rehandle.RehandleEventCommand;
import lib.starter.event.snapshot.MakeDataSnapshotCommand;
import lib.starter.pointer.PointerKeeper;
import lib.starter.task.waiting.WaitForDataCommand;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InternalCommandsConfiguration {

    @Bean
    @ConditionalOnMissingBean
    InternalCommandContext internalCommandContext() {
        final InternalCommandContext internalCommandContext = new InternalCommandContext();
        internalCommandContext.register("RehandleEvent", RehandleEventCommand.class);
        internalCommandContext.register("MakeDataSnapshot", MakeDataSnapshotCommand.class);
        internalCommandContext.register("WaitForData", WaitForDataCommand.class);
        return internalCommandContext;
    }

    @Bean
    @ConditionalOnMissingBean
    InternalCommandPublisher internalCommandPublisher(final InternalCommandContext internalCommandContext,
                                                      final ObjectMapper objectMapper,
                                                      final InternalCommandRepository repository) {
        return new InternalCommandPublisher(internalCommandContext, objectMapper, repository);
    }

    @Bean
    @ConditionalOnMissingBean
    CommandPublishingScheduler commandPublishingScheduler(final InternalCommandContext commandContext,
                                                          final InternalCommandRepository repository,
                                                          final ApplicationEventPublisher publisher,
                                                          final ObjectMapper objectMapper,
                                                          final PointerKeeper pointerKeeper,
                                                          @Value("${starter.eventsourcing.command.publishing.package-size}") final int packageSize) {
        return new CommandPublishingScheduler(commandContext, repository, publisher, objectMapper, pointerKeeper, packageSize);
    }
}
