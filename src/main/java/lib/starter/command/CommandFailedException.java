package lib.starter.command;

import lombok.Getter;

import java.util.UUID;

@Getter
public class CommandFailedException extends RuntimeException {

    private final UUID commandId;

    public CommandFailedException(final UUID commandId) {
        super("The goal of command '" + commandId + "' was not achieved!");
        this.commandId = commandId;
    }
}
