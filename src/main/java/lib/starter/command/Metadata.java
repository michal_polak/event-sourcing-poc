package lib.starter.command;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.ZonedDateTime;
import java.util.UUID;

@Getter
@RequiredArgsConstructor
public class Metadata {
    private final UUID commandId;
    private final String aggregateId;
    private final Long aggregateVersion;
    private final ZonedDateTime occurredOn;
    private final Long index;
}
