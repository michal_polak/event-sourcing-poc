package lib.starter.command;


import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;


public abstract class InternalCommandConfigurer {

    @Autowired
    private InternalCommandContext context;

    public abstract void registerInternalCommands(InternalCommandContext context);

    @PostConstruct
    void postConstruct() {
        registerInternalCommands(context);
    }
}