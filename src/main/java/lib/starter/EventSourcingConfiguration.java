package lib.starter;

import lib.starter.command.InternalCommandsConfiguration;
import lib.starter.command.reexecute.ReexecuteConfiguration;
import lib.starter.event.internal.InternalEventsConfiguration;
import lib.starter.event.multi.MultiEventHandlerConfiguration;
import lib.starter.event.rehandle.RehandleConfiguration;
import lib.starter.event.snapshot.SnapshotingConfiguration;
import lib.starter.event.upcast.UpcastingConfiguration;
import lib.starter.pointer.PointingConfiguration;
import lib.starter.secret.SecretKeysConfiguration;
import lib.starter.sqush.SquashingConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


@Configuration
@Import({SquashingConfiguration.class,
        InternalCommandsConfiguration.class,
        InternalEventsConfiguration.class,
        PointingConfiguration.class,
        SnapshotingConfiguration.class,
        UpcastingConfiguration.class,
        MultiEventHandlerConfiguration.class,
        RehandleConfiguration.class,
        ReexecuteConfiguration.class,
        SecretKeysConfiguration.class
})
public class EventSourcingConfiguration {

    public static final String BASE_PACKAGE = "lib.starter";

}
