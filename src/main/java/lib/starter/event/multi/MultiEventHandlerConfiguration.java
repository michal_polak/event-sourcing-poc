package lib.starter.event.multi;

import com.fasterxml.jackson.databind.ObjectMapper;
import lib.starter.event.internal.InternalEventContext;
import lib.starter.pointer.PointerKeeper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MultiEventHandlerConfiguration {


    @Bean
    @ConditionalOnMissingBean
    MultiEventHandlerContext multiEventHandlerContext(
            PointerKeeper pointerKeeper
    ) {
        return new MultiEventHandlerContext(pointerKeeper);
    }


    @Bean
    @ConditionalOnMissingBean
    MultiEventHandlerProcessor multiEventHandlerProcessor(
            MultiEventHandlerContext handlerContext,
            MultiInternalEventRepository repository,
            InternalEventContext eventContext,
            ObjectMapper objectMapper
    ) {
        return new MultiEventHandlerProcessor(handlerContext,
                repository,
                eventContext,
                objectMapper);
    }
}
