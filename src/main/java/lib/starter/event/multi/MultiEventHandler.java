package lib.starter.event.multi;

import lib.starter.event.internal.AbstractInternalEvent;
import lib.starter.pointer.PointerKeeper;
import lib.starter.pointer.PointerName;
import lib.starter.task.waiting.ContractId;

import java.util.List;

public abstract class MultiEventHandler {

    private PointerKeeper pointerKeeper;

    protected abstract void handle(List<AbstractInternalEvent> events);

    protected abstract List<String> getEventNames();

    void updateStartIndex(Long index, String aggregateId) {
        final String determinant = ContractId.Extractor.forClass(this.getClass()) + " : " + aggregateId;
        pointerKeeper.update(PointerName.MULTI_EVENT_HANDLER, index, determinant);
    }

    void setPointerKeeper(PointerKeeper pointerKeeper) {
        this.pointerKeeper = pointerKeeper;
    }

    Long getStartIndex(String aggregateId) {
        final String determinant = ContractId.Extractor.forClass(this.getClass()) + " : " + aggregateId;
        return pointerKeeper.findOrCreatePointerIndex(PointerName.MULTI_EVENT_HANDLER, determinant);
    }

}
