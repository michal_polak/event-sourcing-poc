package lib.starter.event.multi;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

public abstract class MultiEventHandlerConfigurer {

    @Autowired
    private MultiEventHandlerContext context;

    public abstract void registerHandlers(MultiEventHandlerContext context);

    @PostConstruct
    void postConstruct() {
        registerHandlers(context);
    }
}
