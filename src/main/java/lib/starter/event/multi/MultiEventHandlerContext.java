package lib.starter.event.multi;

import lib.starter.pointer.PointerKeeper;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@RequiredArgsConstructor
public class MultiEventHandlerContext {

    private final PointerKeeper pointerKeeper;

    @Getter
    private Set<MultiEventHandler> handlers = new HashSet<>();

    public void register(MultiEventHandler handler) {
        handler.setPointerKeeper(pointerKeeper);
        handlers.add(handler);
    }
}
