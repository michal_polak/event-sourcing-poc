package lib.starter.event.multi;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface MultiInternalEventRepository extends JpaRepository<MultiInternalEventEntity, UUID> {
    List<MultiInternalEventEntity> findByAggregateIdAndNameInAndIndexBetween(String aggregateId, List<String> eventNames, Long startIndex, Long endIndex);
}
