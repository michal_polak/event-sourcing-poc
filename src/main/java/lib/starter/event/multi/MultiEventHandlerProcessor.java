package lib.starter.event.multi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lib.starter.event.internal.AbstractInternalEvent;
import lib.starter.event.internal.InternalEventContext;
import lib.starter.event.internal.Metadata;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
public class MultiEventHandlerProcessor {

    private final MultiEventHandlerContext handlerContext;
    private final MultiInternalEventRepository repository;
    private final InternalEventContext eventContext;
    private final ObjectMapper objectMapper;

    @Async
    @EventListener
    public void handle(final AbstractInternalEvent event) {
        handlerContext.getHandlers().forEach(h -> {
            final String aggregateId = event.getAggregateId();
            final List<MultiInternalEventEntity> eventEntities = findEvents(aggregateId, h.getEventNames(), h.getStartIndex(aggregateId), event.getEventIndex());
            if (eventEntities.isEmpty()) {
                h.updateStartIndex(event.getEventIndex(), aggregateId);
            }
            if (containsAllRequiredEvents(h, eventEntities)) {
                final List<AbstractInternalEvent> events = eventEntities.stream().map(this::toAbstractInternalEvent).collect(Collectors.toList());
                h.handle(events);
                final Long newStartIndex = nextStartIndexForProcessing(events);
                h.updateStartIndex(newStartIndex, aggregateId);
            }
        });
    }

    private Long nextStartIndexForProcessing(List<AbstractInternalEvent> events) {
        return events.stream().map(AbstractInternalEvent::getEventIndex).max(Long::compare).orElse(0L) + 1L;
    }

    private boolean containsAllRequiredEvents(MultiEventHandler h, List<MultiInternalEventEntity> eventEntities) {
        return eventEntities.stream().map(MultiInternalEventEntity::getName).collect(Collectors.toList()).containsAll(h.getEventNames());
    }

    private AbstractInternalEvent toAbstractInternalEvent(MultiInternalEventEntity eventEntity) {
        try {
            final Class<? extends AbstractInternalEvent> clazz = eventContext.findClass(eventEntity.getName());
            final AbstractInternalEvent event;
            event = objectMapper.readValue(eventEntity.getPayload(), clazz);
            final Metadata metaData = new Metadata(eventEntity.getName(), eventEntity.getAggregateId(), eventEntity.getAggregateVersion(), eventEntity.getOccurredOn(), eventEntity.getIndex());
            event.includeMetadata(metaData);
            return event;
        } catch (JsonProcessingException e) {
            log.error("Event not include! Problem with deserialize from JSON.", e);
        }
        return null;
    }

    private List<MultiInternalEventEntity> findEvents(String aggregateId, List<String> eventNames, final Long startIndex, final Long endIndex) {
        return repository.findByAggregateIdAndNameInAndIndexBetween(aggregateId, eventNames, startIndex, endIndex);
    }


}
