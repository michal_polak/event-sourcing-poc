package lib.starter.event.multi;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.ZonedDateTime;
import java.util.UUID;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "MultiInternalEventEntity")
@Table(name = "internal_event")
class MultiInternalEventEntity {

    @Id
    private UUID id;

    @Column
    private String name;

    @Column
    private String aggregateId;

    @Column
    private Long aggregateVersion;

    @Column
    private ZonedDateTime occurredOn;

    @Column
    private String payload;

    @Column
    private ZonedDateTime creationDate;

    @Column
    private Long index;

}
