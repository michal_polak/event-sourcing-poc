package lib.starter.event.upcast;

public interface ModificationStrategy {

    void applyModification(final MutableEventData externalEvent);
}
