package lib.starter.event.upcast;


import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;


public abstract class EventUpcastConfigurer {

    @Autowired
    private EventUpcastContext context;

    public abstract void registerUpcasters(final EventUpcastContext context);

    @PostConstruct
    void postConstruct() {
        registerUpcasters(context);
    }
}
