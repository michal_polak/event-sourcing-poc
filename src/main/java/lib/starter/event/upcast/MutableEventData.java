package lib.starter.event.upcast;


import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;
import fake.application.infra.event.external.AbstractExternalEvent;
import fake.application.infra.event.external.ExternalEvents;

import java.time.ZonedDateTime;
import java.util.Map;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MutableEventData {

    private String eventName;
    private String aggregateId;
    private Long aggregateVersion;
    private ZonedDateTime occurredOn;
    private Map<String, Object> payloadMap;

    public static MutableEventData of(ObjectMapper objectMapper, AbstractExternalEvent externalEvent) {
        final Map<String, Object> payloadMap = objectMapper.convertValue(externalEvent, Map.class);
        final String aggregateId = externalEvent.getAggregateId();
        final Long targetVersion = externalEvent.getAggregateVersion();
        final ZonedDateTime occurredOn = externalEvent.getMetaData().getOccurredOn();
        final String eventName = ExternalEvents.findNameByClass(externalEvent.getClass());


        return MutableEventData.builder()
                .payloadMap(payloadMap)
                .aggregateId(aggregateId)
                .aggregateVersion(targetVersion)
                .occurredOn(occurredOn)
                .eventName(eventName)
                .build();
    }
}
