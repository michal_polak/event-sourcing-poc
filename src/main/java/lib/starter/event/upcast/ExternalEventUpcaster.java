package lib.starter.event.upcast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;


@Slf4j
@RequiredArgsConstructor
public class ExternalEventUpcaster {

    private final EventUpcastContext eventUpcastContext;
    private final UpcastInternalEventRepository repository;
    private final ObjectMapper objectMapper;

    @Async
    @EventListener
    void catchAndUpcast(final UpcastableEvent upcastableEvent) {
        try {
            final Class<? extends UpcastableEvent> eventClazz = upcastableEvent.getClass();
            final EventUpcaster eventUpcaster = eventUpcastContext.findByClass(eventClazz);
            final MutableEventData eventData = eventUpcaster.upcast(upcastableEvent);
            final UpcastInternalEventEntity internalEventEntity = UpcastInternalEventEntity.of(objectMapper, eventData);
            repository.saveAndFlush(internalEventEntity);
        } catch (final JsonProcessingException e) {
            log.error("Upcasting failed for external event with id " + upcastableEvent.getAggregateId(), e);
        }
    }
}
