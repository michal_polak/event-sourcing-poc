package lib.starter.event.upcast;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UseFieldValueAsTargetIdStrategy implements ModificationStrategy {

    private final String aggregateIdField;

    @Override
    public void applyModification(MutableEventData eventData) {
        final String aggregateId = String.valueOf(eventData.getPayloadMap().get(aggregateIdField));
        eventData.setAggregateId(aggregateId);
    }
}
