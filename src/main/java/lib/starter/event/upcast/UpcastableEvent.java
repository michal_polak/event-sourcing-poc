package lib.starter.event.upcast;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface UpcastableEvent {

    @JsonIgnore
    String getAggregateId();

    @JsonIgnore
    Long getAggregateVersion();
}
