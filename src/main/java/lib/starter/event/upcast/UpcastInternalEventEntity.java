package lib.starter.event.upcast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.UUID;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "UpcastInternalEventEntity")
@Table(name = "internal_event")
public class UpcastInternalEventEntity {

    @Id
    private UUID id;

    private String name;

    @Column
    private String aggregateId;

    @Column
    private Long aggregateVersion;

    @Column
    private ZonedDateTime occurredOn;

    @Column
    private String payload;

    @Column
    private ZonedDateTime creationDate;

    @Column(updatable = false, insertable = false)
    private Long index;

    public static UpcastInternalEventEntity of(ObjectMapper mapper, MutableEventData eventData) throws JsonProcessingException {
        final String payload = mapper.writeValueAsString(eventData.getPayloadMap());
        return UpcastInternalEventEntity.builder().
                payload(payload)
                .aggregateId(eventData.getAggregateId())
                .aggregateVersion(eventData.getAggregateVersion())
                .occurredOn(eventData.getOccurredOn())
                .name(eventData.getEventName())
                .build();
    }

    @PrePersist
    private void prePersist() {
        id = UUID.randomUUID();
        creationDate = ZonedDateTime.now();
    }

}
