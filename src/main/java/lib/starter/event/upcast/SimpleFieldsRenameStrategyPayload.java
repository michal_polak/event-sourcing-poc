package lib.starter.event.upcast;

import lombok.RequiredArgsConstructor;
import org.springframework.data.util.Pair;

import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
public class SimpleFieldsRenameStrategyPayload implements ModificationStrategy {

    private final List<Pair<String, String>> overrides;

    public void applyModification(final MutableEventData eventData) {
        overrides.forEach(override -> {
            final Map<String, Object> payloadMap = eventData.getPayloadMap();
            final Object value = payloadMap.get(override.getFirst());
            payloadMap.put(override.getSecond(), value);
            payloadMap.remove(override.getFirst());
        });
    }
}
