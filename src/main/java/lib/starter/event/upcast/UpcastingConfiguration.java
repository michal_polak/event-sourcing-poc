package lib.starter.event.upcast;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UpcastingConfiguration {

    @Bean
    @ConditionalOnMissingBean
    EventUpcastContext eventUpcastContext() {
        return new EventUpcastContext();
    }

    @Bean
    @ConditionalOnMissingBean
    ExternalEventUpcaster ExternalEventUpcaster(
            final EventUpcastContext eventUpcastContext,
            final UpcastInternalEventRepository repository,
            final ObjectMapper objectMapper) {
        return new ExternalEventUpcaster(eventUpcastContext, repository, objectMapper);
    }
}
