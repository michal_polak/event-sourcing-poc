package lib.starter.event.upcast;

import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
public class EventUpcastContext {

    final Map<Class<? extends UpcastableEvent>, EventUpcaster<? extends UpcastableEvent>> upcasters = new HashMap<>();

    public <U extends EventUpcaster<E>, E extends UpcastableEvent> void register(final Class<E> eventClazz, final U upcaster) {
        upcasters.put(eventClazz, upcaster);
    }

    public EventUpcaster findByClass(Class<? extends UpcastableEvent> eventClazz) {
        return upcasters.get(eventClazz);
    }
}
