package lib.starter.event.upcast;

public interface EventUpcaster<T extends UpcastableEvent> {

    MutableEventData upcast(T externalEvent);
}
