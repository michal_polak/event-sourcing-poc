package lib.starter.event.upcast;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
interface UpcastInternalEventRepository extends JpaRepository<UpcastInternalEventEntity, UUID> {
}
