package lib.starter.event.internal;


import com.fasterxml.jackson.databind.ObjectMapper;
import lib.starter.event.snapshot.DataSnapshotCreatedEvent;
import lib.starter.pointer.PointerKeeper;
import lib.starter.task.waiting.MissingDataProvidedInternalEvent;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InternalEventsConfiguration {

    @Bean
    @ConditionalOnMissingBean
    InternalEventContext internalEventContext() {
        final InternalEventContext context = new InternalEventContext();
        context.register("MissingDataProvided", MissingDataProvidedInternalEvent.class);
        context.register("DataSnapshotCreated", DataSnapshotCreatedEvent.class);
        return context;

    }

    @Bean
    @ConditionalOnMissingBean
    InternalEventPublisher internalEventPublisher(final InternalEventContext internalEventContext,
                                                  final ObjectMapper objectMapper,
                                                  final InternalEventRepository repository
    ) {
        return new InternalEventPublisher(internalEventContext, objectMapper, repository);
    }

    @Bean
    @ConditionalOnMissingBean
    InternalEventPublishingScheduler internalEventPublishingScheduler(final InternalEventContext eventContext,
                                                                      final PointerKeeper pointerKeeper,
                                                                      final InternalEventRepository repository,
                                                                      final ApplicationEventPublisher publisher,
                                                                      final ObjectMapper objectMapper,
                                                                      @Value("${starter.eventsourcing.event.publishing.package-size}") final int packageSize) {
        return new InternalEventPublishingScheduler(eventContext, pointerKeeper, repository, publisher, objectMapper, packageSize);
    }

}
