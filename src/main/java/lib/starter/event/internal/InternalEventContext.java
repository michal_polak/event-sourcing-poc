package lib.starter.event.internal;

import java.util.HashMap;
import java.util.Map;

public class InternalEventContext {

    final Map<String, Class<? extends AbstractInternalEvent>> mapping = new HashMap<>();

    public void register(String eventName, Class<? extends AbstractInternalEvent> clazz) {
        mapping.put(eventName, clazz);
    }

    public String findName(Class<? extends AbstractInternalEvent> clazz) {
        return mapping.entrySet().stream().filter(e -> e.getValue() == clazz).findFirst().map(Map.Entry::getKey).orElse(null);
    }

    public Class<? extends AbstractInternalEvent> findClass(String eventName) {
        return mapping.get(eventName);
    }
}
