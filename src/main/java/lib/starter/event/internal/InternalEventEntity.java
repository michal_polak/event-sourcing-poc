package lib.starter.event.internal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.UUID;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "InternalEventEntity")
@Table(name = "internal_event")
public class InternalEventEntity {

    @Id
    private UUID id;

    @Column(insertable = false, updatable = false)
    private Long index;

    @Column
    private String name;

    @Column
    private String aggregateId;

    @Column
    private Long aggregateVersion;

    @Column
    private ZonedDateTime occurredOn;

    @Column
    private String payload;

    @Column
    private ZonedDateTime creationDate;

    public static InternalEventEntity of(final String aggregateId, final Long aggregateVersion, final String payload, final String eventName) {
        return InternalEventEntity.builder()
                .aggregateId(aggregateId)
                .aggregateVersion(aggregateVersion)
                .payload(payload)
                .occurredOn(ZonedDateTime.now())
                .name(eventName)
                .build();
    }

    @PrePersist
    private void prePersist() {
        id = UUID.randomUUID();
        creationDate = ZonedDateTime.now();
    }

    public Metadata metaData() {
        return new Metadata(name, aggregateId, aggregateVersion, occurredOn, index);
    }
}
