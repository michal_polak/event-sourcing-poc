package lib.starter.event.internal;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class InternalEventPublisher {

    private final InternalEventContext internalEventContext;
    private final ObjectMapper objectMapper;
    private final InternalEventRepository repository;

    public void publishEvent(final String aggregateId, final Long aggregateVersion, final AbstractInternalEvent event) {
        try {
            final String payload = objectMapper.writeValueAsString(event);
            final String eventName = internalEventContext.findName(event.getClass());
            final InternalEventEntity eventEntity = InternalEventEntity.of(aggregateId, aggregateVersion, payload, eventName);
            repository.saveAndFlush(eventEntity);
        } catch (JsonProcessingException e) {
            log.error("Event not published! Problem with serialize to JSON.", e);
        }
    }

    public void publishEvent(final String aggregateId, final Long aggregateVersion, final Object eventData, String eventName) {
        try {
            final String payload = objectMapper.writeValueAsString(eventData);
            final InternalEventEntity eventEntity = InternalEventEntity.of(aggregateId, aggregateVersion, payload, eventName);
            repository.save(eventEntity);
        } catch (JsonProcessingException e) {
            log.error("Event not published! Problem with serialize to JSON.", e);
        }
    }
}