package lib.starter.event.internal;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lib.starter.pointer.PointerKeeper;
import lib.starter.pointer.PointerName;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@RequiredArgsConstructor
public class InternalEventPublishingScheduler {
    private final InternalEventContext eventContext;
    private final PointerKeeper pointerKeeper;
    private final InternalEventRepository repository;
    private final ApplicationEventPublisher publisher;
    private final ObjectMapper objectMapper;
    private final AtomicLong pointer = new AtomicLong();
    private final int packageSize;

    @Scheduled(
            initialDelayString = "${starter.eventsourcing.event.publishing.init-delay}",
            fixedRateString = "${starter.eventsourcing.event.publishing.fixed-rate}"
    )
    void run() {
        final Pageable pageable = PageRequest.of(0, packageSize, Sort.by("index"));
        final Page<InternalEventEntity> page = repository.findByIndexIsGreaterThan(pointer.longValue(), pageable);
        final List<InternalEventEntity> eventEntities = page.getContent();
        eventEntities.forEach(ee -> {
            try {
                final Class<? extends AbstractInternalEvent> clazz = eventContext.findClass(ee.getName());
                final AbstractInternalEvent event = objectMapper.readValue(ee.getPayload(), clazz);
                final Metadata metaData = ee.metaData();
                event.includeMetadata(metaData);
                publisher.publishEvent(event);
                log.info("Internal event '" + ee.getName() + "' published.");
                pointer.set(ee.getIndex());
            } catch (JsonProcessingException e) {
                log.error("Event contract broken!", e);
            }
        });
        pointerKeeper.update(PointerName.INTERNAL_EVENT_QUEUE, pointer);
    }

    @PostConstruct
    private void postConstruct() {
        final Long startIndex = findOrCreatePointerIndex();
        pointer.set(startIndex);
    }

    private Long findOrCreatePointerIndex() {
        return pointerKeeper.findOrCreatePointerIndex(PointerName.INTERNAL_EVENT_QUEUE);
    }
}
