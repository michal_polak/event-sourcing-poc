package lib.starter.event.internal;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

public abstract class InternalEventConfigurer {

    @Autowired
    private InternalEventContext context;

    public abstract void registerInternalEvents(InternalEventContext context);

    @PostConstruct
    void postConstruct() {
        registerInternalEvents(context);
    }
}
