package lib.starter.event.internal;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class AbstractInternalEvent {

    @JsonIgnore
    private Metadata metadata;

    public void includeMetadata(final Metadata metaData) {
        this.metadata = metaData;
    }

    @JsonIgnore
    public String getAggregateId() {
        return metadata.getAggregateId();
    }

    @JsonIgnore
    public Long getAggregateVersion() {
        return metadata.getAggregateVersion();
    }

    @JsonIgnore
    public Long getEventIndex() {
        return metadata.getIndex();
    }

    @JsonIgnore
    public String getEventName() {
        return metadata.getName();
    }
}
