package lib.starter.event.snapshot;


import lib.starter.event.internal.InternalEventContext;
import lib.starter.event.internal.InternalEventPublisher;
import lib.starter.sqush.DataProvider;
import lib.starter.sqush.SquashedData;
import lib.starter.task.waiting.ContractId;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;

import java.util.List;

@RequiredArgsConstructor
public class MakeDataSnapshotCommandExecutor {

    private final InternalEventContext eventContext;
    private final DataProvider dataProvider;
    private final InternalEventPublisher publisher;
    private final SnapshotRepository repository;
    private final List<String> packagesToScan;


    @Async
    @EventListener
    void handle(final MakeDataSnapshotCommand command) {
        final String aggregateId = command.getAggregateId();
        final Long indexToSnapshot = command.getIndexToSnapshot();
        final String contractId = command.getContractId();
        final Class<?> clazz = ContractId.Scanner.classByContractId(contractId, packagesToScan).orElseThrow(() -> new RuntimeException("Class with id " + contractId + " not found!"));
        makeSnapshot(aggregateId, indexToSnapshot, clazz);
    }

    private void makeSnapshot(final String aggregateId, final Long eventIndex, final Class<?> clazz) {
            final SquashedData<?> squashedData = dataProvider.getDataAtIndex(aggregateId, eventIndex, clazz);
            final SnapshotEntity snapshot = SnapshotEntity.of(squashedData);
            repository.save(snapshot);
            final String commandName = eventContext.findName(DataSnapshotCreatedEvent.class);
            publisher.publishEvent(squashedData.getAggregateId(), squashedData.getAggregateVersion(), squashedData.getData(), commandName);
    }
}
