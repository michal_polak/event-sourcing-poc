package lib.starter.event.snapshot;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@RequiredArgsConstructor
public class SnapshotDataProvider {
    final SnapshotRepository repository;

    public Optional<SnapshotData> getLastSnapshot(String aggregateId) {
        Optional<SnapshotEntity> optionalSnapshot = repository.findOneByAggregateIdOrderByLastIndex(aggregateId);
        return optionalSnapshot.map(se -> SnapshotData.of(se));
    }

}
