package lib.starter.event.snapshot;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface SnapshotRepository extends JpaRepository<SnapshotEntity, UUID> {
    Optional<SnapshotEntity> findOneByAggregateIdOrderByLastIndex(String aggregateId);
}
