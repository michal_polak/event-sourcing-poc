package lib.starter.event.snapshot;

import lib.starter.serialization.JsonConverter;
import lib.starter.sqush.SquashedData;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.UUID;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "SnapshotEntity")
@Table(name = "snapshot")
public class SnapshotEntity {

    @Id
    private UUID id;

    private String aggregateId;

    private Long firstIndex;

    private Long lastIndex;

    private Long aggregateVersion;

    @Convert(converter = JsonConverter.class)
    private Object data;

    private ZonedDateTime creationDate;

    private ZonedDateTime modificationDate;

    public static SnapshotEntity of(final SquashedData<?> squashedData) {
        return SnapshotEntity.builder().aggregateId(squashedData.getAggregateId())
                .aggregateVersion(squashedData.getAggregateVersion())
                .data(squashedData.getData())
                .firstIndex(squashedData.getFirstIndex())
                .lastIndex(squashedData.getLastIndex())
                .build();

    }

    @PrePersist
    private void prePersist() {
        id = UUID.randomUUID();
        creationDate = ZonedDateTime.now();
        modificationDate = ZonedDateTime.now();
    }

    @PreUpdate
    private void preUpdate() {
        modificationDate = ZonedDateTime.now();
    }
}
