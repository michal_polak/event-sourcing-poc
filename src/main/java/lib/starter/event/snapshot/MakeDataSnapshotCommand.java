package lib.starter.event.snapshot;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.jackson.Jacksonized;
import lib.starter.command.AbstractInternalCommand;

@Getter
@Builder
@Jacksonized
@RequiredArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class MakeDataSnapshotCommand extends AbstractInternalCommand {

    @JsonProperty("@originalCause")
    private final String originalCause;

    @JsonProperty("@contractId")
    private final String contractId;

    @JsonProperty("@indexToSnapshot")
    private final Long indexToSnapshot;
}
