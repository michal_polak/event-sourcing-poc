package lib.starter.event.snapshot;

import lib.starter.event.internal.InternalEventContext;
import lib.starter.event.internal.InternalEventPublisher;
import lib.starter.sqush.DataProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.boot.autoconfigure.AutoConfigurationPackages;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@RequiredArgsConstructor
public class SnapshotingConfiguration {

    private final BeanFactory beanFactory;

    @Bean
    @ConditionalOnMissingBean
    SnapshotDataProvider snapshotDataProvider(final SnapshotRepository repository) {
        return new SnapshotDataProvider(repository);
    }

    @Bean
    @ConditionalOnMissingBean
    MakeDataSnapshotCommandExecutor makeDataSnapshotCommandExecutor(final InternalEventContext eventContext,
                                                                    final DataProvider dataProvider,
                                                                    final InternalEventPublisher publisher,
                                                                    final SnapshotRepository repository) {

        final List<String> packagesToScan = AutoConfigurationPackages.get(beanFactory);

        return new MakeDataSnapshotCommandExecutor(
                eventContext,
                dataProvider,
                publisher,
                repository,
                packagesToScan
        );
    }
}
