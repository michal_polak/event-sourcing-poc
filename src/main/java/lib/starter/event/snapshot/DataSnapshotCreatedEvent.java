package lib.starter.event.snapshot;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lib.starter.event.internal.AbstractInternalEvent;
import lib.starter.sqush.SquashedData;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.util.Map;

@Getter
@Builder
@Jacksonized
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataSnapshotCreatedEvent extends AbstractInternalEvent {

    @JsonIgnore
    private final Map<String, Object> data;

    @JsonAnyGetter
    public Map<String, Object> getData() {
        return data;
    }

    @JsonCreator
    public DataSnapshotCreatedEvent(final Map<String, Object> data) {
        this.data = data;
    }

    public DataSnapshotCreatedEvent(final SquashedData<Map<String, Object>> squashedData) {
        this(squashedData.getData());
    }

}
