package lib.starter.event.snapshot;

import lib.starter.sqush.IndexRange;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class SnapshotData {

    final Long aggregateVersion;

    final IndexRange indexRange;

    public static SnapshotData of(final SnapshotEntity se) {
        return SnapshotData.builder()
                .aggregateVersion(se.getAggregateVersion())
                .indexRange(IndexRange.of(se.getFirstIndex(), se.getLastIndex()))
                .build();
    }

    public Long getLastEventIndex() {
        return indexRange.getLast();
    }
}
