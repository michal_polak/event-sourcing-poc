package lib.starter.event.rehandle;

import com.fasterxml.jackson.databind.ObjectMapper;
import lib.starter.event.internal.InternalEventContext;
import lib.starter.event.internal.InternalEventRepository;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class RehandleConfiguration {


    @Bean
    RehandleEventCommandExecutor rehandleEventCommandExecutor(final InternalEventContext internalEventContext,
                                                              final InternalEventRepository internalEventRepository,
                                                              final ObjectMapper objectMapper,
                                                              final ApplicationContext applicationContext
    ) {

        return new RehandleEventCommandExecutor(
                internalEventContext,
                internalEventRepository,
                objectMapper,
                applicationContext
        );

    }


}
