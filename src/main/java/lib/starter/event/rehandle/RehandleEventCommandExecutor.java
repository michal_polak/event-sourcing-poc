package lib.starter.event.rehandle;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lib.starter.event.internal.*;
import lib.starter.task.waiting.ContractId;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.transaction.event.TransactionalEventListener;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@RequiredArgsConstructor
public class RehandleEventCommandExecutor {

    private final InternalEventContext internalEventContext;
    private final InternalEventRepository internalEventRepository;
    private final ObjectMapper objectMapper;
    private final ApplicationContext applicationContext;

    @EventListener
    public void execute(final RehandleEventCommand command) throws IllegalAccessException, InvocationTargetException, JsonProcessingException {
        final InternalEventEntity eventEntity = internalEventRepository.findById(command.getEventId()).orElseThrow(() -> new RuntimeException("Event not found!"));
        final Class<? extends AbstractInternalEvent> clazz = internalEventContext.findClass(eventEntity.getName());
        final AbstractInternalEvent event = objectMapper.readValue(eventEntity.getPayload(), clazz);
        final Metadata metaData = eventEntity.metaData();
        event.includeMetadata(metaData);
        final Optional<Class<?>> optionalHandlerClass = ContractId.Scanner.classByContractId(command.getHandlerContractId(), List.of("fake.application", "lib.starter"));

        if (optionalHandlerClass.isPresent()) {
            final Class<?> handlerClass = optionalHandlerClass.get();
            final Object handler = applicationContext.getBean(handlerClass);
            final Method handlingMethod = findHandingMethod(command, eventEntity, clazz, handlerClass);
            handlingMethod.invoke(handler, event);
        }
    }

    private Method findHandingMethod(RehandleEventCommand command, InternalEventEntity eventEntity, Class<? extends AbstractInternalEvent> clazz, Class<?> handlerClass) {
        return Arrays.stream(handlerClass.getDeclaredMethods()).filter(m -> isHandlingMethod(clazz, m)).findFirst()
                .orElseThrow(() -> new IllegalStateException("Not found handling method for event " +
                        "'" + eventEntity.getName() + "' in handler '" + command.getHandlerContractId() + "'."));
    }

    private boolean isHandlingMethod(Class<? extends AbstractInternalEvent> clazz, Method m) {
        return Arrays.asList(m.getParameterTypes()).contains(clazz) && m.getParameterCount() == 1 &&
                (m.getAnnotationsByType(EventListener.class).length > 0
                        || m.getAnnotationsByType(TransactionalEventListener.class).length > 0);
    }
}
