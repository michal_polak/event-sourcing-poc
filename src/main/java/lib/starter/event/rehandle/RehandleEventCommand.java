package lib.starter.event.rehandle;

import com.fasterxml.jackson.annotation.JsonProperty;
import lib.starter.command.AbstractInternalCommand;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@Getter
@RequiredArgsConstructor
public class RehandleEventCommand extends AbstractInternalCommand {

    @JsonProperty("@eventId")
    private final UUID eventId;

    @JsonProperty("@handlerContractId")
    private final String handlerContractId;
}
