package lib.starter.secret;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.UUID;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "GdprKeyEntity")
@Table(name = "secret_key_pair")
public class SecretKeysEntity {

    @Id
    private UUID id;

    @Column
    private String userId;

    @Column
    private String publicKey;

    @Column
    private String privateKey;

    @Version
    private Long version;

    @CreationTimestamp
    private ZonedDateTime creationDate;

    @UpdateTimestamp
    private ZonedDateTime modificationDate;


    @PrePersist
    private void prePersist() {
        id = UUID.randomUUID();
        creationDate = ZonedDateTime.now();
        modificationDate = ZonedDateTime.now();
    }

    @PreUpdate
    private void preUpdate() {
        modificationDate = ZonedDateTime.now();
    }

    public void dropPrivateKey() {
        privateKey = null;
    }
}
