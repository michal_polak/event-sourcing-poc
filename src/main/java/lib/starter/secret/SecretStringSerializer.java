package lib.starter.secret;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

public class SecretStringSerializer extends StdSerializer<SecretString> {

    @Autowired
    private SecretService secretService;

    public SecretStringSerializer() {
        this(null);
    }

    public SecretStringSerializer(Class<SecretString> t) {
        super(t);
    }

    @Override
    public void serialize(SecretString value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        final String secretString = secretService.encrypt(value.getKeyId(), value.getValue());
        gen.writeString(secretString);
    }
}
