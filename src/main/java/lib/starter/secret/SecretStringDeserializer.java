package lib.starter.secret;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

public class SecretStringDeserializer extends StdDeserializer<SecretString> {

    @Autowired
    private SecretService secretService;

    public SecretStringDeserializer() {
        this(null);
    }

    public SecretStringDeserializer(Class<SecretString> t) {
        super(t);
    }

    @Override
    public SecretString deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        final String secret = jsonParser.readValueAs(String.class);
        return secretService.decrypt(secret);
    }

}
