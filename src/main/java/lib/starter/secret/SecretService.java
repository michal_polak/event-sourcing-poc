package lib.starter.secret;

import lombok.RequiredArgsConstructor;
import org.springframework.util.Base64Utils;

import javax.crypto.Cipher;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
public class SecretService {

    private final SecretKeysRepository secretKeysRepository;

    private SecretKeysEntity generateUserKeyPair(String userId) {
        final KeyPair keyPair = generateKeyPair(userId);
        final PrivateKey privateKey = keyPair.getPrivate();
        final String privateKeyBase64 = Base64Utils.encodeToString(privateKey.getEncoded());
        final PublicKey publicKey = keyPair.getPublic();
        final String publicKeyBase64 = Base64Utils.encodeToString(publicKey.getEncoded());
        return secretKeysRepository.save(SecretKeysEntity.builder().userId(userId).privateKey(privateKeyBase64).publicKey(publicKeyBase64).build());
    }

    public String encrypt(UUID keyPairId, String text) {
        final PublicKey publicKey = getPublicKey(keyPairId);
        final byte[] raw = encryptText(publicKey, text);
        final String secretAsBase64 = Base64Utils.encodeToString(raw);
        return "secret:" + keyPairId + "." + secretAsBase64;
    }

    private byte[] encryptText(Key publicKey, String text) {
        try {
            Cipher rsa = Cipher.getInstance("RSA");
            rsa.init(Cipher.ENCRYPT_MODE, publicKey);
            return rsa.doFinal(text.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    PublicKey getPublicKey(UUID keyId) {
        try {
            final String publicKeyAsBase64 = findPublicKeyById(keyId);
            final byte[] key = Base64Utils.decodeFromString(publicKeyAsBase64);
            final KeyFactory factory = KeyFactory.getInstance("RSA");
            final X509EncodedKeySpec encodedKeySpec = new X509EncodedKeySpec(key);
            return factory.generatePublic(encodedKeySpec);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String findPublicKeyById(UUID id) {
        final SecretKeysEntity gdprKey = secretKeysRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Not found"));
        return gdprKey.getPublicKey();
    }

    public SecretString decrypt(String magicString) {
        final String[] parts = magicString.split("[:\\.]");
        final UUID keyId = UUID.fromString(parts[1]);
        final PrivateKey privateKey = getPrivateKey(keyId);
        final byte[] encrypted = Base64Utils.decodeFromString(parts[2]);
        if (privateKey != null) {
            final byte[] decrypted = decryptText(privateKey, encrypted);
            return new SecretString(keyId, new String(decrypted, StandardCharsets.UTF_8));
        } else {
            return new SecretString(keyId, "Anonymous");
        }
    }

    private byte[] decryptText(PrivateKey privateKey, byte[] rawSecret) {
        try {
            Cipher rsa = Cipher.getInstance("RSA");
            rsa.init(Cipher.DECRYPT_MODE, privateKey);
            return rsa.doFinal(rawSecret);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    PrivateKey getPrivateKey(UUID keyId) {
        try {
            final String publicKeyAsBase64 = findPrivateKeyById(keyId);
            final byte[] encodedKey = Base64Utils.decodeFromString(publicKeyAsBase64);
            KeyFactory factory = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec encodedKeySpec = new PKCS8EncodedKeySpec(encodedKey);
            return factory.generatePrivate(encodedKeySpec);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private String findPrivateKeyById(UUID id) {
        final SecretKeysEntity gdprKey = secretKeysRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Not found"));
        return gdprKey.getPrivateKey();
    }

    private KeyPair generateKeyPair(String userId) {
        try {
            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(2048);
            return kpg.generateKeyPair();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public UUID getOrGenerateKeyPair(String userId) {
        final List<SecretKeysEntity> keyPairs = secretKeysRepository.findAllByUserId(userId);
        if (keyPairs.isEmpty()) {
            return generateUserKeyPair(userId).getId();
        } else {
            return keyPairs.get(0).getId();
        }
    }

    public void dropPrivateKeys(String userId) {
        secretKeysRepository.findAllByUserId(userId).forEach(
                keyPair -> {
                    keyPair.dropPrivateKey();
                    secretKeysRepository.saveAndFlush(keyPair);
                });
    }
}
