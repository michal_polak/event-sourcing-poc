package lib.starter.secret;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.UUID;

@RequiredArgsConstructor
@JsonSerialize(using = SecretStringSerializer.class)
@JsonDeserialize(using = SecretStringDeserializer.class)
final public class SecretString {

    @Getter
    @JsonIgnore
    private final UUID keyId;

    @Getter
    @JsonIgnore
    private final String value;

    public static SecretString copy(SecretString other) {
        return new SecretString(other.getKeyId(), other.getValue());
    }

    @Override
    public String toString() {
        return "*****";
    }

    boolean valueEquals(String other) {
        if (value != null) {
            return value.equals(other);
        }
        return false;
    }
}
