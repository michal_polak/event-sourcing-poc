package lib.starter.secret;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SecretKeysConfiguration {

    @Bean
    SecretService secretService(SecretKeysRepository gdprKeysRepository) {
        return new SecretService(gdprKeysRepository);
    }

}
