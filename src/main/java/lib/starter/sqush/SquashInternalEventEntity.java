package lib.starter.sqush;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.UUID;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "SquashInternalEventEntity")
@Table(name = "internal_event")
public class SquashInternalEventEntity {

    @Id
    private UUID id;

    @Column
    private String name;

    @Column
    private String aggregateId;

    @Column
    private Long aggregateVersion;

    @Column
    private ZonedDateTime occurredOn;

    @Column
    private String payload;

    @Column
    private ZonedDateTime creationDate;

    @Column
    private Long index;

}
