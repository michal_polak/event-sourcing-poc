package lib.starter.sqush;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
interface SquashInternalEventRepository extends JpaRepository<SquashInternalEventEntity, UUID> {

    List<SquashInternalEventEntity> findAllByAggregateId(String aggregateId);

    Optional<SquashInternalEventEntity> findOneByAggregateIdAndNameAndAggregateVersionLessThanOrderByAggregateVersion(String aggregateId, String eventName, Long aggregateVersion);

    List<SquashInternalEventEntity> findAllByAggregateIdIsAndAggregateVersionLessThanEqual(String aggregateId, Long aggregateVersion);

    List<SquashInternalEventEntity> findAllByAggregateIdIsAndIndexGreaterThanEqual(String aggregateId, Long aggregateVersion);

    List<SquashInternalEventEntity> findAllByAggregateIdIsAndIndexLessThanEqual(String aggregateId, Long eventIndex);
}
