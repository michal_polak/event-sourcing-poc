package lib.starter.sqush;

import java.util.List;

public interface DataSquasher {

    <T> T squashToObject(List<SquashInternalEventEntity> events, Class<T> clazz);
    <T> SquashedData<T> squash(List<SquashInternalEventEntity> events, Class<T> clazz);
}
