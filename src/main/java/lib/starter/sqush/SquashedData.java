package lib.starter.sqush;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class SquashedData<T> {
    private final String aggregateId;
    private final Long aggregateVersion;
    private final Long firstIndex;
    private final Long lastIndex;
    private final T data;

    public SquashedData(final SquashKneader kneader, final Class<T> clazz) {
        aggregateId = kneader.getAggregateId();
        aggregateVersion = kneader.getAggregateVersion();
        firstIndex = kneader.getIndexRange().getFirst();
        lastIndex = kneader.getIndexRange().getLast();
        data = kneader.getDataAs(clazz);
    }
}
