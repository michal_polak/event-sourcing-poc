package lib.starter.sqush;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SquashKneaderFactory {

    private final SquashingContext squashingContext;

    private final ObjectMapper objectMapper;

    public DefaultSquashKneader defaultKneader() {
        return new DefaultSquashKneader(objectMapper);
    }

    public <T> SquashKneader createKneader(Class<T> clazz) {
        if (squashingContext.exists(clazz)) {
            return squashingContext.findKneaderCreator(clazz).create();
        }
        return defaultKneader();
    }
}
