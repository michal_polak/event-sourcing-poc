package lib.starter.sqush;

import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class DefaultDataSquasher implements DataSquasher {

    private final SquashKneaderFactory squashKneaderFactory;

    @Override
    public <T> T squashToObject(final List<SquashInternalEventEntity> events, Class<T> clazz) {
        return squash(events, clazz).getData();
    }

    @Override
    public <T> SquashedData<T> squash(final List<SquashInternalEventEntity> events, Class<T> clazz) {
        final SquashKneader squashKneader = squashKneaderFactory.createKneader(clazz);
        events.forEach(squashKneader::knead);
        return new SquashedData<T>(squashKneader, clazz);
    }
}
