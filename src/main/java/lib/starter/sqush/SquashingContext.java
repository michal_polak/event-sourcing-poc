package lib.starter.sqush;

import java.util.HashMap;
import java.util.Map;

public class SquashingContext {

    private final Map<Class<?>, SquashKneaderCreator> kneaderCreators = new HashMap<>();

    public void register(final Class<?> dataClazz, final SquashKneaderCreator kneaderCreator) {
        kneaderCreators.put(dataClazz, kneaderCreator);
    }

    public <T> SquashKneaderCreator findKneaderCreator(Class<T> clazz) {
        return kneaderCreators.get(clazz);
    }

    public <T> boolean exists(Class<T> clazz) {
        return kneaderCreators.get(clazz) != null;
    }
}
