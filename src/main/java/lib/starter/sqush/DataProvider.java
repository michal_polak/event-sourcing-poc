package lib.starter.sqush;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class DataProvider {
    private static final String DATA_SNAPSHOT_EVENT_NAME = "DataSnapshotCreated";

    private final ObjectMapper objectMapper;
    private final DefaultDataSquasher squasher;
    private final SquashInternalEventRepository repository;

    public <T> SquashedData<T> getCurrentData(final String aggregateId, final Class<T> clazz) {
        final List<SquashInternalEventEntity> events = repository.findAllByAggregateId(aggregateId);
        return squasher.squash(events, clazz);
    }

    public <T> SquashedData<T> getCurrentData(final String aggregateId, final Long aggregateVersion, final Class<T> clazz) {
        final Optional<SquashInternalEventEntity> optionalSnapshotEvent = findOptimalSnapshot(aggregateId, aggregateVersion);
        if (optionalSnapshotEvent.isPresent()) {
            final SquashInternalEventEntity snapshotEvent = optionalSnapshotEvent.get();
            final List<SquashInternalEventEntity> events = findEventsBySnapshot(aggregateId, snapshotEvent);
            return squasher.squash(events, clazz);
        } else {
            final List<SquashInternalEventEntity> events = repository.findAllByAggregateId(aggregateId);
            return squasher.squash(events, clazz);
        }
    }

    private List<SquashInternalEventEntity> findEventsBySnapshot(String aggregateId, SquashInternalEventEntity snapshotEvent) {
        return repository.findAllByAggregateIdIsAndIndexGreaterThanEqual(aggregateId, snapshotEvent.getIndex());
    }

    private Optional<SquashInternalEventEntity> findOptimalSnapshot(String aggregateId, Long aggregateVersion) {
        return repository.findOneByAggregateIdAndNameAndAggregateVersionLessThanOrderByAggregateVersion(aggregateId, DATA_SNAPSHOT_EVENT_NAME, aggregateVersion);
    }

    public <T> SquashedData<T> getSquashedDataAtVersion(final String aggregateId, final Long version, final Class<T> clazz) {
        final List<SquashInternalEventEntity> events = repository.findAllByAggregateIdIsAndAggregateVersionLessThanEqual(aggregateId, version);
        return squasher.squash(events, clazz);
    }

    public <T> T getObjectAtVersion(final String aggregateId, final Long version, final Class<T> clazz) {
        final List<SquashInternalEventEntity> events = repository.findAllByAggregateIdIsAndAggregateVersionLessThanEqual(aggregateId, version);
        return squasher.squashToObject(events, clazz);
    }

    public <T> T getObjectAtIndex(String aggregateId, Long eventIndex, Class<T> clazz) {
        final List<SquashInternalEventEntity> events = repository.findAllByAggregateIdIsAndIndexLessThanEqual(aggregateId, eventIndex);
        return squasher.squashToObject(events, clazz);
    }

    public <T> SquashedData<T> getDataAtIndex(String aggregateId, Long eventIndex, Class<T> clazz) {
        final List<SquashInternalEventEntity> events = repository.findAllByAggregateIdIsAndIndexLessThanEqual(aggregateId, eventIndex);
        return squasher.squash(events, clazz);
    }
}
