package lib.starter.sqush;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

public abstract class SquashingConfigurer {

    @Autowired
    private SquashingContext context;

    public abstract void registerSquashKneader(SquashingContext context);

    @PostConstruct
    void postConstruct() {
        registerSquashKneader(context);
    }
}
