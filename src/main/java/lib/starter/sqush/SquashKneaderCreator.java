package lib.starter.sqush;

public interface SquashKneaderCreator {
    SquashKneader create();
}
