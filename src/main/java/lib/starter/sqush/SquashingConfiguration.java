package lib.starter.sqush;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SquashingConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public SquashingContext squashingContext() {
        return new SquashingContext();
    }

    @Bean
    @ConditionalOnMissingBean
    public SquashKneaderFactory squashKneaderFactory(final SquashingContext squashingContext,
                                                     final ObjectMapper objectMapper) {
        return new SquashKneaderFactory(squashingContext, objectMapper);
    }

    @Bean
    @ConditionalOnMissingBean
    public DefaultDataSquasher defaultDataSquasher(final SquashKneaderFactory squashKneaderFactory) {
        return new DefaultDataSquasher(squashKneaderFactory);
    }

    @Bean
    @ConditionalOnMissingBean
    public DataProvider dataProvider(final ObjectMapper objectMapper,
                                     final DefaultDataSquasher defaultDataSquasher,
                                     final SquashInternalEventRepository repository) {
        return new DataProvider(objectMapper, defaultDataSquasher, repository);
    }

}
