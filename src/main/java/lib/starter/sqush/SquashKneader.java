package lib.starter.sqush;

public interface SquashKneader {
    void knead(SquashInternalEventEntity internalEventEntity);

    String getAggregateId();

    Long getAggregateVersion();

    IndexRange getIndexRange();

    <T> T getDataAs(Class<T> clazz);
}
