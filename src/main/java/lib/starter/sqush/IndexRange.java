package lib.starter.sqush;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class IndexRange {
    private Long first;
    private Long last;

    public static IndexRange of(Long firstIndex, Long lastIndex) {
        return new IndexRange(firstIndex, lastIndex);
    }

    void apply(Long value) {
        if (first == null) {
            first = value;
        }
        last = value;
    }

}
