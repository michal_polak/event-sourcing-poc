package lib.starter.sqush;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
public class DefaultSquashKneader implements SquashKneader {

    private final ObjectMapper objectMapper;

    @Getter
    private String aggregateId;

    @Getter
    private Long aggregateVersion;

    @Getter
    private Long numberOfEvents = 0L;

    @Getter
    private final IndexRange indexRange = new IndexRange();

    @Getter
    private final Map<String, Object> data = new HashMap<>();

    public <T> T getDataAs(Class<T> clazz) {
        return objectMapper.convertValue(data, clazz);
    }

    public void knead(final SquashInternalEventEntity e) {
        indexRange.apply(e.getIndex());
        final Map<String, Object> payloadData;
        try {
            payloadData = objectMapper.readValue(e.getPayload(), LinkedHashMap.class);
            this.aggregateId = e.getAggregateId();
            this.aggregateVersion = e.getAggregateVersion();
            payloadData.keySet().forEach(key ->
                    data.put(key, payloadData.get(key))
            );
            this.numberOfEvents++;
        } catch (Exception exception) {
            log.error("Error during squashing for internal event " + e.getId() + "! Event not included!");
        }

    }
}


