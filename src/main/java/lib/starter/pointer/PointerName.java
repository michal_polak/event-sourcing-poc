package lib.starter.pointer;

public enum PointerName {
    INTERNAL_EVENT_QUEUE,
    COMMAND_QUEUE,
    EXTERNAL_EVENT_QUEUE,
    MULTI_EVENT_HANDLER;
}
