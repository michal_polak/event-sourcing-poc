package lib.starter.pointer;

import lombok.RequiredArgsConstructor;

import java.util.concurrent.atomic.AtomicLong;


@RequiredArgsConstructor
public class PointerKeeper {

    private final PointerRepository repository;

    public void update(final PointerName pointerName, AtomicLong pointer) {
        final PointerEntity pointerEntity = getOrCreate(pointerName, null);
        pointerEntity.updateIndex(pointer.get());
        repository.saveAndFlush(pointerEntity);
    }

    public void update(final PointerName pointerName, final Long index, final String determinant) {
        final PointerEntity pointerEntity = getOrCreate(pointerName, determinant);
        pointerEntity.updateIndex(index);
        repository.saveAndFlush(pointerEntity);
    }

    public Long findOrCreatePointerIndex(final PointerName pointerName) {
        return getOrCreate(pointerName, null).getIndex();
    }

    public Long findOrCreatePointerIndex(final PointerName pointerName, String determinant) {
        return getOrCreate(pointerName, determinant).getIndex();
    }

    private PointerEntity getOrCreate(PointerName pointerName, String determinant) {
        if (determinant == null) {
            return repository.findByName(pointerName).orElseGet(() -> PointerEntity.of(pointerName, 0L));
        }
        return repository.findByNameAndDeterminant(pointerName, determinant).orElseGet(() -> PointerEntity.of(pointerName, 0L));
    }
}
