package lib.starter.pointer;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface PointerRepository extends JpaRepository<PointerEntity, UUID> {

    Optional<PointerEntity> findByName(PointerName pointerName);

    Optional<PointerEntity> findByNameAndDeterminant(PointerName pointerName, String determinant);
}
