package lib.starter.pointer;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PointingConfiguration {

    @Bean
    @ConditionalOnMissingBean
    PointerKeeper pointerKeeper(final PointerRepository repository) {
        return new PointerKeeper(repository);
    }
}
