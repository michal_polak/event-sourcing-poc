package lib.starter.pointer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "PointerEntity")
@Table(name = "pointer")
public class PointerEntity {

    @Id
    private UUID id;

    @Column
    @Enumerated(EnumType.STRING)
    private PointerName name;

    @Column
    private String determinant;

    @Column
    private Long index;

    @Column
    private ZonedDateTime creationDate;

    @Column
    private ZonedDateTime modificationDate;

    public static PointerEntity of(final PointerName name, final AtomicLong pointer) {
        return PointerEntity.builder()
                .name(name)
                .index(pointer.longValue())
                .build();
    }

    public static PointerEntity of(final PointerName name, final Long index) {
        return PointerEntity.builder()
                .name(name)
                .index(index)
                .build();
    }

    void updateIndex(long index) {
        this.index = index;
    }

    @PrePersist
    private void prePersist() {
        id = UUID.randomUUID();
        creationDate = ZonedDateTime.now();
        modificationDate = ZonedDateTime.now();
    }

    @PreUpdate
    private void preUpdate() {
        modificationDate = ZonedDateTime.now();
    }


}
