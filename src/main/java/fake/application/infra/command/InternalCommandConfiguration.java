package fake.application.infra.command;

import fake.application.domain.example.reload.ReloadExampleCommand;
import fake.application.domain.example.update.UpdateExampleCommand;
import fake.application.domain.notification.SendNotificationCommand;
import fake.application.domain.user.create.CreateUserCommand;
import lib.starter.command.InternalCommandConfigurer;
import lib.starter.command.InternalCommandContext;
import lib.starter.command.reexecute.ReexecuteCommand;
import org.springframework.context.annotation.Configuration;

@Configuration
class InternalCommandConfiguration extends InternalCommandConfigurer {

    @Override
    public void registerInternalCommands(final InternalCommandContext context) {
        context.register("ReexecuteCommand", ReexecuteCommand.class);
        context.register("UpdateExample", UpdateExampleCommand.class);
        context.register("ReloadExample", ReloadExampleCommand.class);
        context.register("SendNotification", SendNotificationCommand.class);
        context.register("CreateUser", CreateUserCommand.class);

    }

}
