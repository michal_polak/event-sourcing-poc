package fake.application.infra.event.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.ZonedDateTime;

@Getter
@RequiredArgsConstructor
public class MetaData {
    private final String targetId;
    private final Long targetVersion;
    private final ZonedDateTime occurredOn;
    private final Long index;

}
