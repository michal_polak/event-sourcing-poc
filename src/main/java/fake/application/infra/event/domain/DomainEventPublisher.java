package fake.application.infra.event.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class DomainEventPublisher {

    private final DomainEventRepository repository;
    private final ObjectMapper objectMapper;

    public void publish(final String aggregateId, final Long aggregateVersion, final AbstractDomainEvent event) {

        try {
            final DomainEvents type = DomainEvents.findByClass(event.getClass());
            final String payload = objectMapper.writeValueAsString(event);
            final DomainEventEntity eventEntity = DomainEventEntity.of(aggregateId, aggregateVersion, payload, type);
            repository.save(eventEntity);
        } catch (JsonProcessingException e) {
            log.error("Event not published! Problem with serialize to JSON.", e);
        }
    }
}
