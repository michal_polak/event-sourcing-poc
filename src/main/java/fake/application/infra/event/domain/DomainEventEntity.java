package fake.application.infra.event.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.UUID;


@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "DomainEventEntity")
@Table(name = "domain_event")
public class DomainEventEntity {

    @Id
    private UUID id;

    @Enumerated
    private DomainEvents type;

    @Column
    private String targetId;

    @Column
    private Long targetVersion;

    @Column
    private ZonedDateTime occurredOn;

    @Column
    private String payload;

    @Column
    private ZonedDateTime creationDate;

    @Column(updatable = false, insertable = false)
    private Long index;


    public static DomainEventEntity of(final String aggregateId, final Long aggregateVersion, final String payload, final DomainEvents type) {
        return DomainEventEntity.builder()
                .targetId(aggregateId)
                .targetVersion(aggregateVersion)
                .occurredOn(ZonedDateTime.now())
                .payload(payload)
                .type(type)
                .build();
    }

    @PrePersist
    private void prePersist() {
        id = UUID.randomUUID();
        creationDate = ZonedDateTime.now();
    }
}
