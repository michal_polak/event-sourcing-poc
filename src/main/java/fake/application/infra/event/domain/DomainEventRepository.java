package fake.application.infra.event.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface DomainEventRepository extends JpaRepository<DomainEventEntity, UUID> {

}
