package fake.application.infra.event.domain;

import fake.application.domain.notification.NotificationRequestedEvent;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;


@RequiredArgsConstructor
public enum DomainEvents { // TODO Auto-register all events implemented AbstractDomainEvent or specific annotation

    PassiveAggressiveNotification(NotificationRequestedEvent.class);

    private final Class<? extends AbstractDomainEvent> clazz;

    public static Class<? extends AbstractDomainEvent> findPayloadClass(final String eventName) {
        return Arrays.stream(values())
                .filter(v -> eventName.equals(v.name()))
                .map(v -> v.clazz).findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Not found payload class for domain event " + eventName));
    }

    public static String findNameByClass(Class<? extends AbstractDomainEvent> clazz) {
        return Arrays.stream(values())
                .filter(v -> v.clazz.equals(clazz))
                .map(Enum::name).findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Not found payload name for domain event class " + clazz.getSimpleName()));
    }

    public static DomainEvents findByClass(Class<? extends AbstractDomainEvent> clazz) {
        return Arrays.stream(values())
                .filter(v -> v.clazz.equals(clazz)).findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Not found payload name for event class " + clazz.getSimpleName()));
    }
}
