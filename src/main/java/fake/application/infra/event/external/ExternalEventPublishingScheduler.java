package fake.application.infra.event.external;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lib.starter.pointer.PointerKeeper;
import lib.starter.pointer.PointerName;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@Component
@RequiredArgsConstructor
public class ExternalEventPublishingScheduler {

    private final PointerKeeper pointerKeeper;
    private final ExternalEventRepository repository;
    private final ApplicationEventPublisher publisher;
    private final ObjectMapper objectMapper;
    private final AtomicLong pointer = new AtomicLong();

    @Scheduled(initialDelay = 100, fixedRate = 100)
    void run() {
        final Pageable pageable = PageRequest.of(0, 10, Sort.by("index"));
        final Page<ExternalEventEntity> eventEntities = repository.findByIndexIsGreaterThan(pointer.longValue(), pageable);
        eventEntities.forEach(ee -> {
            try {
                final Class<? extends AbstractExternalEvent> clazz = ExternalEvents.findPayloadClass(ee.getType());
                final AbstractExternalEvent event = objectMapper.readValue(ee.getPayload(), clazz);
                final MetaData metaData = ee.metaData();
                event.includeMetadata(metaData);
                publisher.publishEvent(event);
                pointer.set(ee.getIndex());
            } catch (JsonProcessingException e) {
                log.error("External event contract is broken!", e);
            }
        });
        pointerKeeper.update(PointerName.EXTERNAL_EVENT_QUEUE, pointer);
    }

    @PostConstruct
    private void postConstruct() {
        final Long startIndex = findOrCreatePointerIndex();
        pointer.set(startIndex);
    }

    private Long findOrCreatePointerIndex() {
        return pointerKeeper.findOrCreatePointerIndex(PointerName.COMMAND_QUEUE);
    }
}
