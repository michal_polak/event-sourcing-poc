package fake.application.infra.event.external;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.UUID;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "ExternalEventEntity")
@Table(name = "external_event")
public class ExternalEventEntity {

    @Id
    private UUID id;

    @Column
    private Long index;

    @Enumerated(EnumType.STRING)
    private ExternalEvents type;

    @Column
    private String targetId;

    @Column
    private Long targetVersion;

    @Column
    private ZonedDateTime occurredOn;

    @Column
    private String payload;

    @Column
    private ZonedDateTime creationDate;

    public static ExternalEventEntity of(final String aggregateId, final Long aggregateVersion, final String payload, final String name) {
        return ExternalEventEntity.builder()
                .targetId(aggregateId)
                .targetVersion(aggregateVersion)
                .payload(payload)
                .occurredOn(ZonedDateTime.now())
                .build();
    }

    @PrePersist
    private void prePersist() {
        id = UUID.randomUUID();
        creationDate = ZonedDateTime.now();
    }

    public MetaData metaData() {
        return new MetaData(id, targetId, targetVersion, occurredOn, index);
    }
}
