package fake.application.infra.event.external;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lib.starter.event.upcast.UpcastableEvent;
import lombok.Getter;

@Getter
public class AbstractExternalEvent implements UpcastableEvent {

    @JsonIgnore
    private MetaData metaData;

    void includeMetadata(MetaData metaData) {
        this.metaData = metaData;
    }

    @JsonIgnore
    public String getAggregateId() {
        return metaData.getAggregateId();
    }

    @JsonIgnore
    public Long getAggregateVersion() {
        return metaData.getAggregateVersion();
    }
}
