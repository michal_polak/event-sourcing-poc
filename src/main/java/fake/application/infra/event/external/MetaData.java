package fake.application.infra.event.external;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.ZonedDateTime;
import java.util.UUID;

@Getter
@RequiredArgsConstructor
public class MetaData {
    private final UUID eventId;
    private final String aggregateId;
    private final Long aggregateVersion;
    private final ZonedDateTime occurredOn;
    private final Long index;

}
