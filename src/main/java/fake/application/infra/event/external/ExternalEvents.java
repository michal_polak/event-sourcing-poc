package fake.application.infra.event.external;

import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@RequiredArgsConstructor
public enum ExternalEvents { // TODO Auto-register all events implemented AbstractExternalEvent or specific annotation

    ExampleCreated(ExampleCreatedExternalEvent.class),
    ExampleChanged(ExampleChangedExternalEvent.class),
    ExampleActivated(ExampleActivatedExternalEvent.class),
    ExampleRemoved(ExampleRemovedExternalEvent.class);

    private final Class<? extends AbstractExternalEvent> clazz;

    public static Class<? extends AbstractExternalEvent> findPayloadClass(final String eventName) {
        return Arrays.stream(values())
                .filter(v -> v.name().equals(eventName))
                .map(v -> v.clazz).findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Not found payload class for event " + eventName));
    }

    public static Class<? extends AbstractExternalEvent> findPayloadClass(final ExternalEvents eventType) {
        return Arrays.stream(values())
                .filter(v -> v == eventType)
                .map(v -> v.clazz).findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Not found payload class for event " + eventType));
    }

    public static String findNameByClass(Class<? extends AbstractExternalEvent> clazz) {
        return Arrays.stream(values())
                .filter(v -> v.clazz.equals(clazz))
                .map(Enum::name).findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Not found payload name for event class " + clazz.getSimpleName()));
    }

    public static ExternalEvents findByClass(Class<? extends AbstractExternalEvent> clazz) {
        return Arrays.stream(values())
                .filter(v -> v.clazz.equals(clazz))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Not found payload name for event class " + clazz.getSimpleName()));
    }
}
