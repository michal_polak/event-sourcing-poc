package fake.application.infra.event.external;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.util.UUID;

@Getter
@Builder
@Jacksonized
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExampleActivatedExternalEvent extends AbstractExternalEvent {
    private final UUID exampleId;
    private final String name;
    private final String status;
    private final String firstName;
    private final String surname;
}
