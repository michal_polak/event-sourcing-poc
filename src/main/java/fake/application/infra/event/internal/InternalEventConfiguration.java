package fake.application.infra.event.internal;

import fake.application.domain.example.activate.ExampleActivatedInternalEvent;
import fake.application.domain.example.change.ExampleChangedInternalEvent;
import fake.application.domain.example.created.ExampleCreatedInternalEvent;
import fake.application.domain.example.reload.ExampleReloadedInternalEvent;
import fake.application.domain.example.remove.ExampleRemovedInternalEvent;
import fake.application.domain.user.create.UserCreatedInternalEvent;
import lib.starter.event.internal.InternalEventConfigurer;
import lib.starter.event.internal.InternalEventContext;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InternalEventConfiguration extends InternalEventConfigurer {

    public void registerInternalEvents(final InternalEventContext context) {
        context.register("ExampleCreated", ExampleCreatedInternalEvent.class);
        context.register("ExampleActivated", ExampleActivatedInternalEvent.class);
        context.register("ExampleChanged", ExampleChangedInternalEvent.class);
        context.register("ExampleRemoved", ExampleRemovedInternalEvent.class);
        context.register("ExampleReloaded", ExampleReloadedInternalEvent.class);
        context.register("UserCreated", UserCreatedInternalEvent.class);
    }

}
