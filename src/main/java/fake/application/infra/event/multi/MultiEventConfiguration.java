package fake.application.infra.event.multi;

import fake.application.domain.example.complete.ProcessCompleteHandler;
import lib.starter.event.multi.MultiEventHandlerConfigurer;
import lib.starter.event.multi.MultiEventHandlerContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MultiEventConfiguration extends MultiEventHandlerConfigurer {

    @Autowired
    private ProcessCompleteHandler completeHandler;

    @Override
    public void registerHandlers(MultiEventHandlerContext context) {
        context.register(completeHandler);
    }
}
