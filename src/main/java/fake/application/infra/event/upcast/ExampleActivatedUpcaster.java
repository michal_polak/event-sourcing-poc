package fake.application.infra.event.upcast;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import fake.application.infra.event.external.ExampleActivatedExternalEvent;
import lib.starter.event.upcast.EventUpcaster;
import lib.starter.event.upcast.MutableEventData;

import java.util.Map;

@Component
@RequiredArgsConstructor
public class ExampleActivatedUpcaster implements EventUpcaster<ExampleActivatedExternalEvent> {

   final ObjectMapper objectMapper;

    @Override
    public MutableEventData upcast(final ExampleActivatedExternalEvent externalEvent) {
        final MutableEventData mutableEvent = MutableEventData.of(objectMapper, externalEvent);
        final Map<String, Object> payload = mutableEvent.getPayloadMap();
        mutableEvent.setAggregateId((String) payload.get("exampleId"));
        return mutableEvent;
    }
}
