package fake.application.infra.event.upcast;

import com.fasterxml.jackson.databind.ObjectMapper;
import fake.application.infra.event.external.ExampleCreatedExternalEvent;
import lib.starter.event.upcast.EventUpcaster;
import lib.starter.event.upcast.MutableEventData;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@RequiredArgsConstructor
public class ExampleCreatedUpcaster implements EventUpcaster<ExampleCreatedExternalEvent> {

    private final ObjectMapper objectMapper;

    @Override
    public MutableEventData upcast(final ExampleCreatedExternalEvent externalEvent) {
        final MutableEventData mutableEvent = MutableEventData.of(objectMapper, externalEvent);
        final Map<String, Object> payload = mutableEvent.getPayloadMap();
        mutableEvent.setAggregateId((String) payload.get("exampleId"));
        return mutableEvent;
    }
}
