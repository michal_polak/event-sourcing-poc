package fake.application.infra.event.upcast;

import fake.application.infra.event.external.ExampleActivatedExternalEvent;
import fake.application.infra.event.external.ExampleChangedExternalEvent;
import fake.application.infra.event.external.ExampleCreatedExternalEvent;
import lib.starter.event.upcast.EventUpcastConfigurer;
import lib.starter.event.upcast.EventUpcastContext;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import fake.application.infra.event.external.ExampleRemovedExternalEvent;

@Configuration
@RequiredArgsConstructor
public class EventUpcastConfiguration extends EventUpcastConfigurer {

    private final ExampleCreatedUpcaster exampleCreatedUpcaster;
    private final ExampleActivatedUpcaster exampleActivatedUpcaster;
    private final ExampleChangedUpcaster exampleChangedUpcaster;
    private final ExampleRemovedUpcaster exampleRemovedUpcaster;

    @Override
    public void registerUpcasters(final EventUpcastContext context) {
        context.register(ExampleCreatedExternalEvent.class, exampleCreatedUpcaster);
        context.register(ExampleActivatedExternalEvent.class, exampleActivatedUpcaster);
        context.register(ExampleChangedExternalEvent.class, exampleChangedUpcaster);
        context.register(ExampleRemovedExternalEvent.class, exampleRemovedUpcaster);
    }

}
