package fake.application.infra.event.upcast;

import com.fasterxml.jackson.databind.ObjectMapper;
import fake.application.infra.event.external.ExampleChangedExternalEvent;
import lib.starter.event.upcast.EventUpcaster;
import lib.starter.event.upcast.MutableEventData;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@RequiredArgsConstructor
public class ExampleChangedUpcaster implements EventUpcaster<ExampleChangedExternalEvent> {

    final ObjectMapper objectMapper;

    @Override
    public MutableEventData upcast(final ExampleChangedExternalEvent externalEvent) {
        final MutableEventData mutableEvent = MutableEventData.of(objectMapper, externalEvent);
        final Map<String, Object> payload = mutableEvent.getPayloadMap();
        mutableEvent.setAggregateId((String) payload.get("exampleId"));
        return mutableEvent;
    }
}
