package fake.application.infra.event.snapshot;

import lib.starter.command.InternalCommandPublisher;
import lib.starter.event.internal.AbstractInternalEvent;
import lib.starter.event.snapshot.MakeDataSnapshotCommand;
import lib.starter.event.snapshot.SnapshotData;
import lib.starter.event.snapshot.SnapshotDataProvider;
import lib.starter.task.waiting.ContractId;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import fake.application.domain.example.ExampleData;
import fake.application.domain.example.activate.ExampleActivatedInternalEvent;
import fake.application.domain.example.change.ExampleChangedInternalEvent;
import fake.application.domain.example.created.ExampleCreatedInternalEvent;
import fake.application.domain.example.remove.ExampleRemovedInternalEvent;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ExampleSnapshoter {

    private final SnapshotDataProvider snapshotDataProvider;
    private final InternalCommandPublisher commandPublisher;

    @Async
    @EventListener
    public void handle(final AbstractInternalEvent event) {
        if (itIsTimeToMakeSnapshot(event)) {
            final String contractId = ContractId.Extractor.forClass(ExampleData.class);
            final MakeDataSnapshotCommand command = new MakeDataSnapshotCommand(event.getEventName(), contractId, event.getEventIndex());
            commandPublisher.publishCommand(event.getAggregateId(), event.getAggregateVersion(), command);
        }
    }

    private void makeExampleSnapshot(final AbstractInternalEvent event) {
        final String contractId = ContractId.Extractor.forClass(ExampleData.class);
        final MakeDataSnapshotCommand command = new MakeDataSnapshotCommand(event.getEventName(), contractId, event.getEventIndex());
        commandPublisher.publishCommand(event.getAggregateId(), event.getAggregateVersion(), command);
    }


    private boolean itIsTimeToMakeSnapshot(AbstractInternalEvent event) {

        if (event instanceof ExampleCreatedInternalEvent || event instanceof ExampleRemovedInternalEvent) {
            return true;
        }

        if (event instanceof ExampleActivatedInternalEvent) {
            return false;
        }

        if (event instanceof ExampleChangedInternalEvent) {
            final Optional<SnapshotData> lastSnapshot = snapshotDataProvider.getLastSnapshot(event.getAggregateId());
            final Long aggregateVersion = event.getAggregateVersion();
            return lastSnapshot.map(s -> s.getAggregateVersion() + 5 < aggregateVersion).orElse(false);
        }
        return false;
    }

}
