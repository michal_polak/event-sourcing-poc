package fake.application;

import lib.starter.EventSourcingConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableJpaRepositories(basePackages = {
        EventSourcingPocApplication.BASE_PACKAGE,
        EventSourcingConfiguration.BASE_PACKAGE
})
@EntityScan(basePackages = {
        EventSourcingPocApplication.BASE_PACKAGE,
        EventSourcingConfiguration.BASE_PACKAGE
})
@EnableScheduling
@SpringBootApplication
@ComponentScan(EventSourcingPocApplication.BASE_PACKAGE)
@Import({EventSourcingConfiguration.class})
public class EventSourcingPocApplication {

    public static final String BASE_PACKAGE = "fake.application";

    public static void main(String[] args) {
        SpringApplication.run(EventSourcingPocApplication.class, args);
    }

}
