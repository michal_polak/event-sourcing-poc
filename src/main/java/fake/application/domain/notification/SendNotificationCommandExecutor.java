package fake.application.domain.notification;

import lib.starter.task.waiting.ContractId;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@ContractId("SendNotificationCommandExecutor")
public class SendNotificationCommandExecutor {
    private final NotificationSender sender;

    @Async
    @EventListener
    public void execute(final SendNotificationCommand command) {
        sender.sendNotification(command.getAggregateId(), command.getAggregateVersion(), Notification.of(command));
    }

}
