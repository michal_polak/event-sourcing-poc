package fake.application.domain.notification;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.jackson.Jacksonized;
import lib.starter.command.AbstractInternalCommand;

import java.util.Map;

@Getter
@Builder
@Jacksonized
@RequiredArgsConstructor
public class SendNotificationCommand extends AbstractInternalCommand {

    private final String template;
    private final String userId;
    private final Map<String, String> parameters;

}
