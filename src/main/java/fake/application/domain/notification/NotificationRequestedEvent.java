package fake.application.domain.notification;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.jackson.Jacksonized;
import fake.application.infra.event.domain.AbstractDomainEvent;

import java.util.Map;

@Getter
@Builder
@Jacksonized
@RequiredArgsConstructor
public class NotificationRequestedEvent extends AbstractDomainEvent {

    private final String userId;
    private final String template;
    private final Map<String, String> name;
}
