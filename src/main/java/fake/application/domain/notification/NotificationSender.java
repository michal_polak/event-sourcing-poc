package fake.application.domain.notification;

import fake.application.infra.event.domain.AbstractDomainEvent;
import fake.application.infra.event.domain.DomainEventPublisher;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@RequiredArgsConstructor
public class NotificationSender {

    private final DomainEventPublisher publisher;

    public void sendNotification(String aggregateId, Long aggregateVersion, Notification notification) {
        final String userId = notification.getUserId();
        final String template = notification.getTemplate();
        final Map<String, String> parameters = notification.getParameters();
        final AbstractDomainEvent event = new NotificationRequestedEvent(userId, template, parameters);
        publisher.publish(aggregateId, aggregateVersion, event);
    }
}
