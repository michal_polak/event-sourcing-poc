package fake.application.domain.notification;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Map;

@Getter
@RequiredArgsConstructor
public class Notification {

    private final String userId;
    private final String template;
    private final Map<String, String> parameters;

    public static Notification of(final SendNotificationCommand command) {
        return new Notification(command.getUserId(), command.getTemplate(), command.getParameters());
    }
}
