package fake.application.domain.example.client;

import org.springframework.stereotype.Component;
import fake.application.domain.example.ExampleStatus;

import java.util.UUID;

@Component
public class ExampleRestClient {
    public ExampleClientResponse getExample(String aggregateId) {
        try {
            Thread.sleep(1000);
            return ExampleClientResponse.builder()
                    .id(UUID.fromString(aggregateId))
                    .name("325435")
                    .status(ExampleStatus.INACTIVE)
                    .firstName("Jan")
                    .surname("Kowalski")
                    .build();
        } catch (InterruptedException e) {
            throw new RuntimeException("Ups...");
        }
    }
}
