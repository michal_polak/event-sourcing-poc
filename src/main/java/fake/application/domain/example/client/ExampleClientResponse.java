package fake.application.domain.example.client;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import fake.application.domain.example.ExampleStatus;

import java.util.UUID;

@ToString
@EqualsAndHashCode
@Getter
@Builder
public class ExampleClientResponse {
    private final UUID id;
    private final String name;
    private final ExampleStatus status;
    private final String firstName;
    private final String surname;
}
