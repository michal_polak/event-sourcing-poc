package fake.application.domain.example;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lib.starter.task.waiting.ContractId;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.util.UUID;

@Getter
@Builder
@Jacksonized
@JsonIgnoreProperties(ignoreUnknown = true)
@ContractId("ExampleData")
public class ExampleData {

    private final UUID exampleId;
    private final String name;
    private final ExampleStatus status;
    private final String firstName;
    private final String lastName;

}
