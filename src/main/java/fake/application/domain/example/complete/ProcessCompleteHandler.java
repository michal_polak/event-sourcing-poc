package fake.application.domain.example.complete;

import fake.application.domain.example.ExampleData;
import fake.application.domain.notification.SendNotificationCommand;
import lib.starter.command.AbstractInternalCommand;
import lib.starter.command.InternalCommandPublisher;
import lib.starter.event.internal.AbstractInternalEvent;
import lib.starter.event.multi.MultiEventHandler;
import lib.starter.sqush.DataProvider;
import lib.starter.task.waiting.ContractId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
@ContractId("ProcessCompleteHandler")
public class ProcessCompleteHandler extends MultiEventHandler {

    private static final String NOTIFICATION_TEMPLATE = "EXAMPLE_PROCESS_COMPLETED";
    private final DataProvider dataProvider;
    private final InternalCommandPublisher commandPublisher;

    @Override
    protected List<String> getEventNames() {
        return List.of("ExampleCreated", "ExampleActivated", "ExampleRemoved");
    }

    @Override
    protected void handle(List<AbstractInternalEvent> events) {
        final String aggregateId = events.get(0).getAggregateId();
        final Long aggregateVersion = events.stream().map(AbstractInternalEvent::getAggregateVersion).max(Long::compare).orElse(0L);
        final AbstractInternalCommand command = prepareSendNotificationCommand(aggregateId, aggregateVersion);
        commandPublisher.publishCommand(aggregateId, aggregateVersion, command);
    }


    private SendNotificationCommand prepareSendNotificationCommand(String aggregateId, Long aggregateVersion) {
        final ExampleData exampleData = dataProvider.getObjectAtVersion(aggregateId, aggregateVersion, ExampleData.class);
        return SendNotificationCommand.builder()
                .template(NOTIFICATION_TEMPLATE)
                .parameters(Map.of("name", exampleData.getName()))
                .build();
    }

}
