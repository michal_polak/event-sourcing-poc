package fake.application.domain.example.remove;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lib.starter.event.internal.AbstractInternalEvent;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.util.UUID;

@Getter
@Builder
@Jacksonized
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExampleRemovedInternalEvent extends AbstractInternalEvent {

    private final UUID exampleId;

}
