package fake.application.domain.example;

public enum ExampleStatus {
    INACTIVE,
    ACTIVE,
    LOCKED,
    REMOVED
}
