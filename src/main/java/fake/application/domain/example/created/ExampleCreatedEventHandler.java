package fake.application.domain.example.created;


import fake.application.domain.example.ExampleData;
import fake.application.domain.example.reload.ExampleReloadedInternalEvent;
import fake.application.domain.example.reload.ReloadExampleCommand;
import fake.application.domain.notification.SendNotificationCommand;
import lib.starter.command.InternalCommandPublisher;
import lib.starter.event.internal.AbstractInternalEvent;
import lib.starter.sqush.DataProvider;
import lib.starter.sqush.SquashedData;
import lib.starter.task.waiting.ContractId;
import lib.starter.task.waiting.MissingDataProvidedInternalEvent;
import lib.starter.task.waiting.MissingDataUndeliveredInternalEvent;
import lib.starter.task.waiting.WaitForDataCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
@ContractId("ExampleCreatedEventHandler")
public class ExampleCreatedEventHandler {

    private static final String NOTIFICATION_TEMPLATE = "EXAMPLE_CREATED";
    private static final Duration A_MOMENT = Duration.ofSeconds(5);

    private final DataProvider dataProvider;
    private final InternalCommandPublisher commandPublisher;

    @Async
    @EventListener
    public void handle(final ExampleCreatedInternalEvent event) {
        final Long aggregateVersion = event.getAggregateVersion();
        final String aggregateId = event.getAggregateId();
        final ExampleData exampleData = dataProvider.getObjectAtVersion(aggregateId, aggregateVersion, ExampleData.class);

        if (!hasRequiredData(exampleData)) {
            final String eventName = event.getEventName();
            final String contractId = ContractId.Extractor.forClass(ExampleData.class);
            final WaitForDataCommand waitForDataCommand = new WaitForDataCommand(A_MOMENT, eventName, List.of("name"), contractId);
            commandPublisher.publishCommand(event.getAggregateId(), event.getAggregateVersion(), waitForDataCommand);
        } else {
            final SendNotificationCommand sendNotificationCommand = prepareSendNotificationCommand(event);
            commandPublisher.publishCommand(event.getAggregateId(), event.getAggregateVersion(), sendNotificationCommand);
        }
    }

    @Async
    @EventListener
    void handle(final MissingDataProvidedInternalEvent event) {
        final String expectedCause = "ExampleCreated";
        final String originalCause = event.getOriginalCause();
        if (expectedCause.equals(originalCause)) {
            final SendNotificationCommand command = prepareSendNotificationCommand(event);
            commandPublisher.publishCommand(event.getAggregateId(), event.getAggregateVersion(), command);
        }

    }

    @Async
    @EventListener
    void handle(final MissingDataUndeliveredInternalEvent event) {
        final String expectedCause = "ExampleCreated";
        final String originalCause = event.getOriginalCause();
        if (expectedCause.equals(originalCause)) {
            final ReloadExampleCommand command = new ReloadExampleCommand(expectedCause, List.of("name"));
            commandPublisher.publishCommand(event.getAggregateId(), event.getAggregateVersion(), command);
        }

    }

    @Async
    @EventListener
    void handle(final ExampleReloadedInternalEvent event) {
        final String expectedCause = "ExampleCreated";
        final String originalCause = event.getOriginalCause();
        if (expectedCause.equals(originalCause)) {
            final SendNotificationCommand sendNotificationCommand = prepareSendNotificationCommand(event);
            commandPublisher.publishCommand(event.getAggregateId(), event.getAggregateVersion(), sendNotificationCommand);
        }
    }

    private SendNotificationCommand prepareSendNotificationCommand(AbstractInternalEvent event) {
        final String aggregateId = event.getAggregateId();
        final SquashedData<ExampleData> squashedData = dataProvider.getCurrentData(aggregateId, ExampleData.class);
        final ExampleData exampleData = squashedData.getData();
        final Map<String, String> parameters = new HashMap<>();
        parameters.put("name", exampleData.getName());
        return SendNotificationCommand.builder()
                .template(NOTIFICATION_TEMPLATE)
                .parameters(parameters)
                .build();
    }


    private boolean hasRequiredData(ExampleData exampleData) {
        return exampleData.getName() != null;
    }

}
