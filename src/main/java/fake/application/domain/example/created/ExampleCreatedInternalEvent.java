package fake.application.domain.example.created;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import fake.application.domain.example.ExampleStatus;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;
import lib.starter.event.internal.AbstractInternalEvent;

import java.util.UUID;

@Getter
@Builder
@Jacksonized
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExampleCreatedInternalEvent extends AbstractInternalEvent {

    private final UUID exampleId;
    private final String name;
    private final ExampleStatus status;
    private final String firstName;
    private final String lastName;

}
