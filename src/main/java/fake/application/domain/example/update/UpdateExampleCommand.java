package fake.application.domain.example.update;

import fake.application.domain.example.ExampleData;
import lib.starter.command.AbstractInternalCommand;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@EqualsAndHashCode(callSuper = true)
@Builder
@RequiredArgsConstructor
public class UpdateExampleCommand extends AbstractInternalCommand {

    private final String originalCause;
    private final ExampleData data;
}
