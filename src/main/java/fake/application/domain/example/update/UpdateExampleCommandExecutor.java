package fake.application.domain.example.update;


import fake.application.domain.example.ExampleData;
import lib.starter.sqush.DataProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UpdateExampleCommandExecutor {

    private final DataProvider dataProvider;
    private final ExampleQueryUpdateRepository repository;

    @Async
    @EventListener
    public void handle(final UpdateExampleCommand command) {
        final ExampleData exampleData = command.getData();
        final ExampleQueryUpdateEntity example = findOrCreateExample(exampleData);
        example.update(exampleData, command.getAggregateVersion());
        repository.save(example);
    }


    private ExampleQueryUpdateEntity findOrCreateExample(ExampleData exampleData) {
        return repository.findById(exampleData.getExampleId())
                .orElseGet(ExampleQueryUpdateEntity::new);
    }

}
