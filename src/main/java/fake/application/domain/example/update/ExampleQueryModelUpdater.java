package fake.application.domain.example.update;


import fake.application.domain.example.ExampleData;
import fake.application.domain.example.created.ExampleCreatedInternalEvent;
import lib.starter.command.InternalCommandPublisher;
import lib.starter.sqush.DataProvider;
import lib.starter.sqush.SquashedData;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ExampleQueryModelUpdater {

    private final DataProvider dataProvider;
    private final InternalCommandPublisher commandPublisher;

    @Async
    @EventListener
    public void handle(final ExampleCreatedInternalEvent trigger) {
        updateQueryModel(trigger);
    }

    private void updateQueryModel(final ExampleCreatedInternalEvent trigger) {
        final SquashedData<ExampleData> squashedData = dataProvider.getCurrentData(trigger.getAggregateId(), ExampleData.class);
        final ExampleData exampleData = squashedData.getData();
        if (shouldBeUpdated(exampleData)) {
            final UpdateExampleCommand command = new UpdateExampleCommand(trigger.getEventName(), exampleData);
            commandPublisher.publishCommand(trigger.getAggregateId(), trigger.getAggregateVersion(), command);
        }
    }

    private boolean shouldBeUpdated(final ExampleData exampleData) {
        return exampleData.getExampleId() != null;
    }

}
