package fake.application.domain.example.update;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import fake.application.domain.example.ExampleData;
import fake.application.domain.example.ExampleStatus;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.UUID;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "ExampleQueryUpdateEntity")
@Table(name = "example")
public class ExampleQueryUpdateEntity {

    @Id
    private UUID id;

    @Column
    private String name;

    @Column
    private ExampleStatus status;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private Long version;

    @Column
    private ZonedDateTime creationDate;

    @Column
    private ZonedDateTime modificationDate;


    public void update(final ExampleData exampleData, final Long aggregateVersion) {
        id = exampleData.getExampleId();
        name = exampleData.getName();
        status = exampleData.getStatus();
        firstName = exampleData.getFirstName();
        lastName = exampleData.getLastName();
        version = aggregateVersion;
    }

    @PrePersist
    private void prePersist() {
        creationDate = ZonedDateTime.now();
        modificationDate = ZonedDateTime.now();
    }

    @PreUpdate
    private void preUpdate() {
        modificationDate = ZonedDateTime.now();
    }
}
