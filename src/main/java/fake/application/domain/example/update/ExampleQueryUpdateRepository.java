package fake.application.domain.example.update;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.UUID;

@Repository
public interface ExampleQueryUpdateRepository extends JpaRepository<ExampleQueryUpdateEntity, UUID> {

}
