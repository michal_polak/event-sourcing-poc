package fake.application.domain.example.details;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ExampleQueryService {

    private final ExampleQueryRepository repository;

    ExampleQueryResponse getExample(final UUID exampleId) {
        return findExample(exampleId).toResponse();
    }

    private ExampleQueryEntity findExample(final UUID exampleId) {
        return repository.findById(exampleId).orElseThrow(() -> new IllegalArgumentException("Not found"));
    }


}
