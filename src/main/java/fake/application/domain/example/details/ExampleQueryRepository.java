package fake.application.domain.example.details;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ExampleQueryRepository extends JpaRepository<ExampleQueryEntity, UUID> {

}
