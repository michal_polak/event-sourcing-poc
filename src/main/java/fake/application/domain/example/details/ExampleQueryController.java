package fake.application.domain.example.details;


import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class ExampleQueryController {

    private final ExampleQueryService queryService;

    @GetMapping
    public ExampleQueryResponse getExample(@PathParam("id") UUID id) {
        return queryService.getExample(id);
    }

}
