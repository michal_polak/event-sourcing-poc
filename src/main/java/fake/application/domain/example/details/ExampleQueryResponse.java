package fake.application.domain.example.details;

import lombok.Builder;
import fake.application.domain.example.ExampleStatus;

import java.util.UUID;

@Builder
public class ExampleQueryResponse {

    private UUID id;

    private String name;

    private ExampleStatus status;

    private String firstName;

    private String lastName;

}
