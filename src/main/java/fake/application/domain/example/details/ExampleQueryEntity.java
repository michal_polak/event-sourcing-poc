package fake.application.domain.example.details;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import fake.application.domain.example.ExampleStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.ZonedDateTime;
import java.util.UUID;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "ExampleQueryEntity")
@Table(name = "example")
public class ExampleQueryEntity {

    @Id
    private UUID id;

    @Column
    private String name;

    @Column
    private ExampleStatus status;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private ZonedDateTime creationDate;

    @Column
    private ZonedDateTime modificationDate;

    @Column
    private Long version;

    public ExampleQueryResponse toResponse() {
        return ExampleQueryResponse.builder()
                .id(id)
                .name(name)
                .status(status)
                .firstName(firstName)
                .lastName(lastName)
                .build();
    }
}
