package fake.application.domain.example.reload;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import fake.application.domain.example.ExampleStatus;
import lib.starter.event.internal.AbstractInternalEvent;

import java.util.List;
import java.util.UUID;

@Getter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExampleReloadedInternalEvent extends AbstractInternalEvent {

    @JsonProperty("@originalCause")
    private final String originalCause;

    @JsonProperty("@fields")
    private final List<String> fields;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final UUID exampleId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String name;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final ExampleStatus status;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String firstName;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final String lastName;
}
