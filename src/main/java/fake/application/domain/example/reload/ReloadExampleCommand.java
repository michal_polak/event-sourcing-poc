package fake.application.domain.example.reload;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lib.starter.command.AbstractInternalCommand;

import java.util.List;

@Getter
@RequiredArgsConstructor
public class ReloadExampleCommand extends AbstractInternalCommand {

    @JsonProperty("@originalCause")
    private final String originalCause;

    @JsonProperty("@fields")
    private final List<String> fields;

}
