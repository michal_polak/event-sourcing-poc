package fake.application.domain.example.reload;

import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import fake.application.domain.example.client.ExampleClientResponse;
import fake.application.domain.example.client.ExampleRestClient;
import lib.starter.event.internal.InternalEventPublisher;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ReloadExampleDataCommandExecutor {

    private ExampleRestClient client;
    private final InternalEventPublisher publisher;

    @Async
    @EventListener
    void execute(final ReloadExampleCommand command) {
        final ExampleClientResponse response = client.getExample(command.getAggregateId());
        final ExampleReloadedInternalEvent event = prepareEvent(command.getFields(), response);
        publisher.publishEvent(command.getAggregateId(), command.getAggregateVersion(), event);
    }


    private ExampleReloadedInternalEvent prepareEvent(List<String> fields, ExampleClientResponse response) {
        final ExampleReloadedInternalEvent.ExampleReloadedInternalEventBuilder builder = ExampleReloadedInternalEvent.builder();
        if (fields.contains("exampleId")) {
            builder.exampleId(response.getId());
        }
        if (fields.contains("name")) {
            builder.name(response.getName());
        }
        if (fields.contains("status")) {
            builder.status(response.getStatus());
        }
        if (fields.contains("firstName")) {
            builder.firstName(response.getFirstName());
        }
        if (fields.contains("lastName")) {
            builder.lastName(response.getSurname());
        }
        return builder.build();
    }
}
