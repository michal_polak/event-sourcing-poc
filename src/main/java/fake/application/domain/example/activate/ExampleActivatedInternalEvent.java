package fake.application.domain.example.activate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;
import fake.application.domain.example.ExampleStatus;
import lib.starter.event.internal.AbstractInternalEvent;

import java.util.UUID;

@Getter
@Builder
@Jacksonized
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExampleActivatedInternalEvent extends AbstractInternalEvent {

    private final UUID exampleId;
    private final ExampleStatus status;
}
