package fake.application.domain.user.create;

import lib.starter.command.AbstractInternalCommand;
import lib.starter.secret.SecretString;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
@Builder
public class CreateUserCommand extends AbstractInternalCommand {

    private final String userId;
    private final SecretString firstName;
    private final SecretString lastName;
}
