package fake.application.domain.user.create;


import lib.starter.secret.SecretString;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class CreateUserSecretRequest {

    private final String userId;
    private final SecretString firstName;
    private final SecretString lastName;

}
