package fake.application.domain.user.create;


import lib.starter.command.AbstractInternalCommand;
import lib.starter.command.InternalCommandPublisher;
import lib.starter.secret.SecretService;
import lib.starter.secret.SecretString;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class CreateUserController {

    private final SecretService secretService;
    private final InternalCommandPublisher commandPublisher;

    @PostMapping("/users")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void createUser(@RequestBody CreateUserRequest request) {
        final UUID keyId = secretService.getOrGenerateKeyPair(request.getUserId());
        final AbstractInternalCommand command = prepareCommand(request, keyId);
        commandPublisher.publishCommand(request.getUserId(), 0L, command);
    }

    @PostMapping(value = "/users", headers = {"Secret=true"})
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void createUser(@RequestBody CreateUserSecretRequest request) {
        final UUID keyId = secretService.getOrGenerateKeyPair(request.getUserId());
        final AbstractInternalCommand command = prepareCommand(request, keyId);
        commandPublisher.publishCommand(request.getUserId(), 0L, command);
    }


    private CreateUserCommand prepareCommand(CreateUserRequest request, UUID keyId) {
        return CreateUserCommand.builder().userId(request.getUserId()).firstName(new SecretString(keyId, request.getFirstName())).lastName(new SecretString(keyId, request.getLastName())).build();
    }

    private CreateUserCommand prepareCommand(CreateUserSecretRequest request, UUID keyId) {
        return CreateUserCommand.builder().userId(request.getUserId()).firstName(request.getFirstName()).lastName(request.getLastName()).build();
    }


}
