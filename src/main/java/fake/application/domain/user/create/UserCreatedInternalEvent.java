package fake.application.domain.user.create;

import lib.starter.event.internal.AbstractInternalEvent;
import lib.starter.secret.SecretString;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
@Builder
public class UserCreatedInternalEvent extends AbstractInternalEvent {

    private final String userId;
    private final SecretString firstName;
    private final SecretString lastName;
}
