package fake.application.domain.user.create;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class CreateUserRequest {

    private final String userId;

    private final String firstName;

    private final String lastName;

}
