package fake.application.domain.user.create;

import lib.starter.command.AbstractInternalCommand;
import lib.starter.event.internal.InternalEventPublisher;
import lib.starter.secret.SecretString;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CreateUserCommandExecutor extends AbstractInternalCommand {

    private final InternalEventPublisher eventPublisher;

    @Async
    @EventListener
    public void execute(CreateUserCommand command) {
        final UserCreatedInternalEvent event = prepareEvent(command);
        final String aggregateId = command.getAggregateId();
        final Long aggregateVersion = command.getAggregateVersion();
        eventPublisher.publishEvent(aggregateId, aggregateVersion, event);
    }

    private UserCreatedInternalEvent prepareEvent(CreateUserCommand command) {
        return UserCreatedInternalEvent.builder().userId(command.getUserId()).firstName(SecretString.copy(command.getFirstName())).lastName(SecretString.copy(command.getLastName())).build();
    }


}
