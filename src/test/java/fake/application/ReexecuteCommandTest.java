package fake.application;

import fake.application.domain.example.details.ExampleQueryController;
import fake.application.domain.notification.SendNotificationCommand;
import fake.application.infra.event.domain.DomainEventEntity;
import fake.application.infra.event.domain.DomainEventRepository;
import fake.application.infra.event.domain.DomainEvents;
import fake.application.infra.event.external.ExternalEventRepository;
import lib.starter.command.InternalCommandEntity;
import lib.starter.command.InternalCommandPublisher;
import lib.starter.command.InternalCommandRepository;
import lib.starter.command.reexecute.ReexecuteCommand;
import lib.starter.event.internal.InternalEventPublisher;
import lib.starter.event.internal.InternalEventRepository;
import lib.starter.event.snapshot.SnapshotRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@SpringBootTest
@Sql("/reexecute-command-test.sql")
class ReexecuteCommandTest {

    @Autowired
    private DomainEventRepository domainEventRepository;

    @Autowired
    private InternalEventRepository internalEventRepository;

    @Autowired
    private InternalCommandRepository internalCommandRepository;

    @Autowired
    private SnapshotRepository snapshotRepository;

    @Autowired
    private InternalEventPublisher eventPublisher;

    @Autowired
    private ExampleQueryController exampleQueryController;

    @Autowired
    private InternalCommandPublisher commandPublisher;

    @Test
    void shouldTwicePublishSentNotificationCommand() throws InterruptedException {
        // given
        final UUID exampleId = UUID.fromString("f8956f2c-676f-4bef-bc7e-412f89fc46b8");
        final UUID aggregateId = exampleId;
        final Long aggregateVersion = 10L;
        final SendNotificationCommand command = SendNotificationCommand.builder().build();

        // when
        commandPublisher.publishCommand(aggregateId.toString(), aggregateVersion, command);
        Thread.sleep(100);
        final InternalCommandEntity commandEntity = internalCommandRepository.findAll().stream().filter(ee -> ee.getName().equals("SendNotification")).findFirst().get();
        final UUID commandId = commandEntity.getId();
        final ReexecuteCommand reexecuteCommand = new ReexecuteCommand(commandId, "SendNotificationCommandExecutor");
        commandPublisher.publishCommand(aggregateId.toString(), aggregateVersion, reexecuteCommand);
        Thread.sleep(2000);

        // then
        final List<DomainEventEntity> domainEventEntities = domainEventRepository.findAll();
        final long numberOfNotifcationRequested = domainEventEntities.stream().filter(c -> c.getType() == DomainEvents.PassiveAggressiveNotification).count();
        assertEquals(2, numberOfNotifcationRequested);
    }
}
