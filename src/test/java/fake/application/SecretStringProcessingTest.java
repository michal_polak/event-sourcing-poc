package fake.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import fake.application.domain.user.create.CreateUserCommand;
import fake.application.domain.user.create.CreateUserController;
import fake.application.domain.user.create.CreateUserRequest;
import fake.application.domain.user.create.UserCreatedInternalEvent;
import lib.starter.command.InternalCommandEntity;
import lib.starter.command.InternalCommandRepository;
import lib.starter.event.internal.InternalEventEntity;
import lib.starter.event.internal.InternalEventRepository;
import lib.starter.secret.SecretService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@ActiveProfiles("test")
@SpringBootTest
@Slf4j
public class SecretStringProcessingTest {

    @Autowired
    private CreateUserController controller;

    @Autowired
    private InternalCommandRepository commandRepository;

    @Autowired
    private InternalEventRepository eventRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private SecretService secretService;

    @SneakyThrows
    @Test
    void shouldEncryptAndDecryptSecretStrings() {
        // given
        final String userId = "35235234";
        final CreateUserRequest request = new CreateUserRequest(userId, "Andrzej", "Dulkiewicz");
        // when
        controller.createUser(request);
        Thread.sleep(2000);
        // then
        Page<InternalCommandEntity> commands = commandRepository.findByIndexIsGreaterThan(0L, Pageable.unpaged());
        final String commandPayload = commands.getContent().get(0).getPayload();
        assertFalse(commandPayload.contains("Andrzej"));
        assertFalse(commandPayload.contains("Dulkiewicz"));
        log.info("Payload: " + commandPayload);

        final CreateUserCommand command = objectMapper.readValue(commandPayload, CreateUserCommand.class);
        assertEquals("Andrzej", command.getFirstName().getValue());
        assertEquals("Dulkiewicz", command.getLastName().getValue());

        Page<InternalEventEntity> events = eventRepository.findByIndexIsGreaterThan(0L, Pageable.unpaged());
        final String eventPayload = events.getContent().get(0).getPayload();
        assertFalse(eventPayload.contains("Andrzej"));
        assertFalse(eventPayload.contains("Dulkiewicz"));
        log.info("Payload: " + eventPayload);

        UserCreatedInternalEvent event = objectMapper.readValue(eventPayload, UserCreatedInternalEvent.class);
        assertEquals("Andrzej", event.getFirstName().getValue());
        assertEquals("Dulkiewicz", event.getLastName().getValue());

        //DROP KEY
        secretService.dropPrivateKeys(userId);
        UserCreatedInternalEvent anonymousEvent = objectMapper.readValue(eventPayload, UserCreatedInternalEvent.class);
        assertEquals("Anonymous", anonymousEvent.getFirstName().getValue());
        assertEquals("Anonymous", anonymousEvent.getLastName().getValue());
    }
}