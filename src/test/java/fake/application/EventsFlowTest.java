package fake.application;

import fake.application.domain.example.details.ExampleQueryController;
import fake.application.domain.example.details.ExampleQueryResponse;
import fake.application.infra.event.domain.DomainEventEntity;
import fake.application.infra.event.domain.DomainEventRepository;
import fake.application.infra.event.external.ExternalEventEntity;
import fake.application.infra.event.external.ExternalEventRepository;
import lib.starter.command.InternalCommandEntity;
import lib.starter.command.InternalCommandRepository;
import lib.starter.event.internal.InternalEventEntity;
import lib.starter.event.internal.InternalEventRepository;
import lib.starter.event.snapshot.SnapshotEntity;
import lib.starter.event.snapshot.SnapshotRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("test")
@SpringBootTest
@Sql("/events-flow-test.sql")
class EventsFlowTest {

    @Autowired
    private ExternalEventRepository externalEventRepository;

    @Autowired
    private InternalEventRepository internalEventRepository;

    @Autowired
    private InternalCommandRepository internalCommandRepository;

    @Autowired
    private SnapshotRepository snapshotRepository;

    @Autowired
    private DomainEventRepository domainEventRepository;

    @Autowired
    private ExampleQueryController exampleQueryController;

    @Test
    void shouldRealizeFullFlow() throws InterruptedException {
        // given
        final UUID aggregateId = UUID.fromString("f8956f2c-676f-4bef-bc7e-412f89fc46b8");
        // when
        Thread.sleep(2000L);

        // then
        final List<ExternalEventEntity> externalEvents = externalEventRepository.findAll();
        assertEquals(1, externalEvents.size());

        final List<InternalEventEntity> internalEvents = internalEventRepository.findAll();
        assertEquals(2, internalEvents.size());

        final List<SnapshotEntity> snapshots = snapshotRepository.findAll();
        assertEquals(1, snapshots.size());

        final List<InternalCommandEntity> commands = internalCommandRepository.findAll();
        assertEquals(3, commands.size());

        final List<DomainEventEntity> domainEvents = domainEventRepository.findAll();
        assertEquals(1, domainEvents.size());

        final ExampleQueryResponse response = exampleQueryController.getExample(aggregateId);
        assertNotNull(response);

    }
}
