package fake.application;

import lib.starter.command.InternalCommandEntity;
import lib.starter.command.InternalCommandRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ActiveProfiles("test")
@SpringBootTest
@Sql("/multi-event-handler-test.sql")
class MultiEventHandlerTest {

    @Autowired
    private InternalCommandRepository internalCommandRepository;

    @Test
    void shouldHandleMultipleEvents() throws InterruptedException {
        // given

        // when
        Thread.sleep(2200L);

        // then
        final List<InternalCommandEntity> commands = internalCommandRepository.findAll();
        boolean result = commands.stream().anyMatch(c -> c.getPayload().contains("EXAMPLE_PROCESS_COMPLETED"));
        assertTrue(result);

    }
}
