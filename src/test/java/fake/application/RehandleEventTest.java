package fake.application;

import fake.application.domain.example.ExampleStatus;
import fake.application.domain.example.created.ExampleCreatedInternalEvent;
import fake.application.domain.example.details.ExampleQueryController;
import fake.application.infra.event.external.ExternalEventRepository;
import lib.starter.command.InternalCommandEntity;
import lib.starter.command.InternalCommandPublisher;
import lib.starter.command.InternalCommandRepository;
import lib.starter.event.internal.InternalEventEntity;
import lib.starter.event.internal.InternalEventPublisher;
import lib.starter.event.internal.InternalEventRepository;
import lib.starter.event.rehandle.RehandleEventCommand;
import lib.starter.event.snapshot.SnapshotRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@SpringBootTest
@Sql("/rehandle-event-test.sql")
class RehandleEventTest {

    @Autowired
    private ExternalEventRepository externalEventRepository;

    @Autowired
    private InternalEventRepository internalEventRepository;

    @Autowired
    private InternalCommandRepository internalCommandRepository;

    @Autowired
    private SnapshotRepository snapshotRepository;

    @Autowired
    private InternalEventPublisher eventPublisher;

    @Autowired
    private ExampleQueryController exampleQueryController;

    @Autowired
    private InternalCommandPublisher commandPublisher;

    @Test
    void shouldTwicePublishSentNotificationCommand() throws InterruptedException {
        // given
        final UUID exampleId = UUID.fromString("f8956f2c-676f-4bef-bc7e-412f89fc46b8");
        final UUID aggregateId = exampleId;
        final Long aggregateVersion = 10L;
        final ExampleCreatedInternalEvent event = ExampleCreatedInternalEvent.builder().exampleId(UUID.randomUUID()).
               name("Test").firstName("Adam").lastName("Rutkowski").status(ExampleStatus.INACTIVE).build();

        // when
        eventPublisher.publishEvent(aggregateId.toString(), aggregateVersion, event);
        Thread.sleep(100);
        final InternalEventEntity eventEntity = internalEventRepository.findAll().stream().filter(ee -> ee.getName().equals("ExampleCreated")).findFirst().get();
        final UUID eventId = eventEntity.getId();
        final RehandleEventCommand rehandleEventCommand = new RehandleEventCommand(eventId, "ExampleCreatedEventHandler");
        commandPublisher.publishCommand(aggregateId.toString(), aggregateVersion, rehandleEventCommand);
        Thread.sleep(2000);

        // then
        final List<InternalCommandEntity> commandEntities = internalCommandRepository.findAll();
        final long countOfNotificationCommands = commandEntities.stream().filter(c -> c.getName().equals("SendNotification")).count();
        assertEquals(2, countOfNotificationCommands);
    }
}
