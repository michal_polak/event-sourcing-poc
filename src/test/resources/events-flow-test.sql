
TRUNCATE TABLE pointer;
TRUNCATE TABLE external_event;
TRUNCATE TABLE internal_event;
TRUNCATE TABLE internal_command;
TRUNCATE TABLE secret_key_pair;
TRUNCATE TABLE example;

INSERT INTO external_event (
  id, type, target_id, target_version, 
  occurred_on, payload, creation_date
) 
VALUES 
  (
    '2ff68d0f-2f6b-4b24-9706-c3d7d8b7be69',
    'ExampleCreated', '85d0164e-359f-11ec-8d3d-0242ac130003',
    0, now(), '{"exampleId":"f8956f2c-676f-4bef-bc7e-412f89fc46b8","name":"Adam","surname":"Kowalski"}',
    now()
  );
