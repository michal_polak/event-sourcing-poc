
TRUNCATE TABLE pointer;
TRUNCATE TABLE external_event;
TRUNCATE TABLE internal_event;
TRUNCATE TABLE internal_command;
TRUNCATE TABLE secret_key_pair;
TRUNCATE TABLE example;

INSERT INTO external_event (
  id, type, target_id, target_version, 
  occurred_on, payload, creation_date
) 
VALUES 
  (
    '2c295ead-7882-46be-8e98-4b96d3915efa',
    'ExampleCreated', '85d0164e-359f-11ec-8d3d-0242ac130003',
    0, now(), '{"exampleId":"f8956f2c-676f-4bef-bc7e-412f89fc46b8","name":"Adam","surname":"Kowalski"}',
    now()
  ),
   (
      'eb3e4fcd-7572-47cc-ab12-2e4926a499a8',
      'ExampleActivated', '85d0164e-359f-11ec-8d3d-0242ac130003',
      1, now(), '{"exampleId":"f8956f2c-676f-4bef-bc7e-412f89fc46b8","status":"ACTIVE","name":"Adam","surname":"Kowalski"}',
      now()
    ),
     (
        '41049687-289d-4e68-a2fd-fd40a73207ca',
        'ExampleRemoved', '85d0164e-359f-11ec-8d3d-0242ac130003',
        2, now(), '{"exampleId":"f8956f2c-676f-4bef-bc7e-412f89fc46b8"}',
        now()
      );
